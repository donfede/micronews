#!/usr/bin/python3
import os
from pprint import pprint
from pypump import PyPump, Client


def verifier(url):
    print('Verify URL: %s' % url)
    return input('Verifier: ')

client = Client(
    webfinger='debian@identi.ca',
    name='export',
    type='native'
)


pump = PyPump(client=client, verifier_callback=verifier)

for activity in pump.me.outbox:
    note = activity.obj

    created = note.published or activity.published
    year = str(created.year)
    month = str(created.month)
    epoch_time = int(created.timestamp())
    created = str(created)
    created = created.replace('+00:00', '')

    if note.object_type == 'comment' or \
        note.object_type != 'note' or \
        note.deleted or \
        note.in_reply_to or \
        not note.content or \
        not note.published or \
        not note.updated or \
        not note.url:
            print("    -> ", activity)
            savedir = "import/{}/no/".format(year)
            if not os.path.exists(savedir):
                os.makedirs(savedir)
            with open(savedir+str(epoch_time), 'w', encoding='UTF-8') as error_f:
                pprint(activity.__dict__, stream=error_f)
                pprint(note.__dict__, stream=error_f)
    else:

        print (activity)
        data = """Title: {title}
Slug: {epoch_time}
Date: {created}
Author: imported from identi.ca/debian
Status: published
identica: {url}

{text}

""".format(title=note.display_name, text=note.content, url=note.url, \
           epoch_time=epoch_time, created=created)

        savedir = "import/{}/{}/".format(year,month)
        if not os.path.exists(savedir):
            os.makedirs(savedir)
        with open(savedir+str(epoch_time)+".md", "w") as f:
            f.write(data)
