#!/usr/bin/python3
"""
Generates a neew feed for identi.ca without the URL in the title

2016-2019, Ana Guerrero Lopez <ana@debian.org>
GPL v2 or any later
"""
import re
import sys
import datetime
from time import mktime
import feedparser
import pytz
from feedgenerator import Atom1Feed


def add_item_to_the_feeds(pump, facebook, item):
    if 'published_parsed' in item:
        mdt = mktime(item['published_parsed'])
    dt = datetime.datetime.fromtimestamp(mdt, pytz.timezone('Europe/Paris'))

    pump.add_item(
        title=re.sub('(https?://\S+)', '', item['title']),
        link=item['link'],
        unique_id=item['id'],
        description=item['summary'],
        author_name=item['author'],
        pubdate=dt)

    # Extract link for facebook: always using first link and
    # use micronews.debian.org link when there isn't any link in the text
    urls = re.findall("(?P<url>https?://[^\s]+)", item['title'])
    if not urls:
        link = item['link']
    else:
        link = urls[0]

    facebook.add_item(
        title=re.sub('(https?://\S+)', '', item['title']),
        link=link,
        unique_id=item['id'],
        description=item['summary'],
        author_name=item['author'],
        pubdate=dt)


final_dir = sys.argv[1]
in_file = final_dir+"/feeds/atom.xml"
pump_file = final_dir+"/feeds/pump.xml"
facebook_file = final_dir+"/feeds/facebook.xml"
d = feedparser.parse(in_file)
entries = d['entries']

site_url = 'https://micronews.debian.org'
feed_domain = site_url
feed_url = 'https://micronews.debian.org/feeds/atom.xml'
FeedClass = Atom1Feed

sitename = 'Debian micronews'
feed_pump = FeedClass(
    title=sitename,
    link=(site_url + '/'),
    feed_url=feed_url,
    description='')

feed_facebook = FeedClass(
    title=sitename,
    link=(site_url + '/'),
    feed_url=feed_url,
    description='')

for i in entries:
    add_item_to_the_feeds(feed_pump, feed_facebook, i)

with open(pump_file, 'w') as fp:
    feed_pump.write(fp, 'utf-8')

with open(facebook_file, 'w') as fp:
    feed_facebook.write(fp, 'utf-8')
