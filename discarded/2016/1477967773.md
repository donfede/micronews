Title: New email lists created for RISC-V porting, Asia-Pacific events and the Colombian community
Slug: 1477967773
Date: 2016-11-01 02:36
Author: Paul Wise
Status: published

New email lists created for [RISC-V porting][], [Asia/Pacific events][] and the [Colombian community][]

[RISC-V porting]: https://lists.debian.org/debian-riscv/
[Asia/Pacific events]: https://lists.debian.org/debian-events-apac/
[Colombian community]: https://lists.debian.org/debian-dug-co/
