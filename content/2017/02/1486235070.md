Title: "First Tails beta release based on Stretch" by Intrigeri https://people.debian.org/~intrigeri/blog/posts/Tails_3.0-beta1_is_out/
Slug: 1486235070
Date: 2017-02-04 19:04
Author: Laura Arjona Reina
Status: published

"First Tails beta release based on Stretch" by Intrigeri [https://people.debian.org/~intrigeri/blog/posts/Tails_3.0-beta1_is_out/](https://people.debian.org/~intrigeri/blog/posts/Tails_3.0-beta1_is_out/)
