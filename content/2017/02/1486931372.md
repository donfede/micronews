Title: "Using FAI to customize and build your own cloud images" by Noah Meyerhans http://noah.meyerhans.us/blog/2017/02/10/using-fai-to-customize-and-build-your-own-cloud-images/
Slug: 1486931372
Date: 2017-02-12 20:29
Author: Laura Arjona Reina
Status: published

"Using FAI to customize and build your own cloud images" by Noah Meyerhans [http://noah.meyerhans.us/blog/2017/02/10/using-fai-to-customize-and-build-your-own-cloud-images/](http://noah.meyerhans.us/blog/2017/02/10/using-fai-to-customize-and-build-your-own-cloud-images/)
