Title: Debian Bug #880000 reported by Salvatore Bonaccorso https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=880000
Slug: 1509468312
Date: 2017-10-31 16:45
Author: Laura Arjona Reina
Status: published

Debian Bug #880000 reported by Salvatore Bonaccorso [https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=880000](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=880000)
