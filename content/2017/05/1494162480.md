Title: "Be careful: Upgrading Debian Jessie to Stretch, with Pacemaker DRBD and an nested ext4 LVM hosted on VMware products" by Patrick Matthäi https://www.linux-dev.org/2017/05/be-careful-upgrading-debian-jessie-to-stretch-with-pacemaker-drbd-and-an-nested-ext4-lvm-hosted-on-vmware-products/
Slug: 1494162480
Date: 2017-05-07 13:08
Author: Laura Arjona Reina
Status: published

"Be careful: Upgrading Debian Jessie to Stretch, with Pacemaker DRBD and an nested ext4 LVM hosted on VMware products" by Patrick Matthäi [https://www.linux-dev.org/2017/05/be-careful-upgrading-debian-jessie-to-stretch-with-pacemaker-drbd-and-an-nested-ext4-lvm-hosted-on-vmware-products/](https://www.linux-dev.org/2017/05/be-careful-upgrading-debian-jessie-to-stretch-with-pacemaker-drbd-and-an-nested-ext4-lvm-hosted-on-vmware-products/)
