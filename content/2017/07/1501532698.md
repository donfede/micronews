Title: DebCamp17 starts today! https://wiki.debconf.org/wiki/DebConf17/DebCamp - and DebConf17 next weekend! https://debconf17.debconf.org
Slug: 1501532698
Date: 2017-07-31 20:24
Author: Laura Arjona Reina
Status: published

DebCamp17 starts today! [https://wiki.debconf.org/wiki/DebConf17/DebCamp](https://wiki.debconf.org/wiki/DebConf17/DebCamp) - and DebConf17 next weekend! [https://debconf17.debconf.org](https://debconf17.debconf.org)
