Title: Debian Project News - September 20, 2017: news about the future of Alioth, Outreachy, Debian Astro 1.0, new translation team: Albanian, "Public Money, Public Code" campaign, and much more https://www.debian.org/News/weekly/2017/03/
Slug: 1506089614
Date: 2017-09-22 14:13
Author: Laura Arjona Reina
Status: published

Debian Project News - September 20, 2017: news about the future of Alioth, Outreachy, Debian Astro 1.0, new translation team: Albanian, "Public Money, Public Code" campaign, and much more [https://www.debian.org/News/weekly/2017/03/](https://www.debian.org/News/weekly/2017/03/)
