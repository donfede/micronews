Title: Work on Debian for mobile devices continues https://bits.debian.org/2017/08/debian-mobile-continues.html
Slug: 1502984186
Date: 2017-08-17 15:36
Author: Laura Arjona Reina
Status: published

Work on Debian for mobile devices continues [https://bits.debian.org/2017/08/debian-mobile-continues.html](https://bits.debian.org/2017/08/debian-mobile-continues.html)
