Title: Next DebConf17 events: Signing package contents, cloud-init, QA BoF https://debconf17.debconf.org/schedule/?day=2017-08-10
Slug: 1502383665
Date: 2017-08-10 16:47
Author: Debian Micronews
Status: published

Next DebConf17 events: Signing package contents, cloud-init, QA BoF [https://debconf17.debconf.org/schedule/?day=2017-08-10](https://debconf17.debconf.org/schedule/?day=2017-08-10)
