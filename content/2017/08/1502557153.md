Title: DebConf17 continues with  Debian on Civil Infrastructure Systems, and Live Demos. Follow the streaming! https://debconf17.debconf.org/schedule/?day=2017-08-12
Slug: 1502557153
Date: 2017-08-12 16:59
Author: Laura Arjona Reina
Status: published

DebConf17 continues with  Debian on Civil Infrastructure Systems, and Live Demos. Follow the streaming! [https://debconf17.debconf.org/schedule/?day=2017-08-12](https://debconf17.debconf.org/schedule/?day=2017-08-12)
