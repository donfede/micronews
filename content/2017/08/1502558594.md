Title: During or right after DebConf17 you can also follow Wikimania: Wikipedia and its sister free knowledge projects conference (Aug 9-13) https://wikimania2017.wikimedia.org/wiki/Wikimania
Slug: 1502558594
Date: 2017-08-12 17:23
Author: Laura Arjona Reina
Status: published

During or right after DebConf17 you can also follow Wikimania: Wikipedia and its sister free knowledge projects conference (Aug 9-13) [https://wikimania2017.wikimedia.org/wiki/Wikimania](https://wikimania2017.wikimedia.org/wiki/Wikimania)
