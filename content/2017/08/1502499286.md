Title: The evening at DebConf17 will end today with a film screening of "The Internet's Own Boy: The Story of Aaron Swartz" https://debconf17.debconf.org/talks/214/
Slug: 1502499286
Date: 2017-08-12 00:54
Author: Laura Arjona Reina
Status: published

The evening at DebConf17 will end today with a film screening of "The Internet's Own Boy: The Story of Aaron Swartz" [https://debconf17.debconf.org/talks/214/](https://debconf17.debconf.org/talks/214/)
