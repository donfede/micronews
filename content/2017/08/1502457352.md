Title: fdisk becoming non-essential, dependencies needed. https://lists.debian.org/debian-devel-announce/2017/08/msg00005.html
Slug: 1502457352
Date: 2017-08-11 13:15
Author: Laura Arjona Reina
Status: published

fdisk becoming non-essential, dependencies needed. [https://lists.debian.org/debian-devel-announce/2017/08/msg00005.html](https://lists.debian.org/debian-devel-announce/2017/08/msg00005.html)
