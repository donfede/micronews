Title: Call for Proposals for DebConf17 Open Day https://bits.debian.org/2017/04/dc17-openday-cfp.html
Slug: 1492506511
Date: 2017-04-18 09:08
Author: Laura Arjona Reina
Status: published

Call for Proposals for DebConf17 Open Day [https://bits.debian.org/2017/04/dc17-openday-cfp.html](https://bits.debian.org/2017/04/dc17-openday-cfp.html)
