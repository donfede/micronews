Title: "Stretch images for Amazon EC2, round 2" by Noah Meyerhans https://noah.meyerhans.us/blog/2017/04/20/stretch-images-for-aws/
Slug: 1492777615
Date: 2017-04-21 12:26
Author: Laura Arjona Reina
Status: published

"Stretch images for Amazon EC2, round 2" by Noah Meyerhans [https://noah.meyerhans.us/blog/2017/04/20/stretch-images-for-aws/](https://noah.meyerhans.us/blog/2017/04/20/stretch-images-for-aws/)
