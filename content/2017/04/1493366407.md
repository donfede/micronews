Title: Stretch is coming soon. Time to plan a Release Party in your city! https://wiki.debian.org/ReleasePartyStretch
Slug: 1493366407
Date: 2017-04-28 08:00
Author: Laura Arjona Reina
Status: published

Stretch is coming soon. Time to plan a Release Party in your city! [https://wiki.debian.org/ReleasePartyStretch](https://wiki.debian.org/ReleasePartyStretch)
