Title: If you use the Debian Live images, consider joining the Debian Live team and help in the development https://lists.debian.org/debian-live/2017/06/msg00064.html
Slug: 1498664633
Date: 2017-06-28 15:43
Author: Laura Arjona Reina
Status: published

If you use the Debian Live images, consider joining the Debian Live team and help in the development [https://lists.debian.org/debian-live/2017/06/msg00064.html](https://lists.debian.org/debian-live/2017/06/msg00064.html)
