Title: stretch-backports and jessie-backports-sloppy are now available http://blog.snow-crash.org/blog/stretch-backports-available/
Slug: 1498849987
Date: 2017-06-30 19:13
Author: Ana Guerrero Lopez
Status: published

stretch-backports and jessie-backports-sloppy are now available [http://blog.snow-crash.org/blog/stretch-backports-available/](http://blog.snow-crash.org/blog/stretch-backports-available/)
