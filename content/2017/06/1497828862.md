Title: Debian GNU/Hurd 2017 released! https://lists.debian.org/debian-hurd/2017/06/msg00017.html
Slug: 1497828862
Date: 2017-06-18 23:34
Author: Ana Guerrero Lopez
Status: published

Debian GNU/Hurd 2017 released! [https://lists.debian.org/debian-hurd/2017/06/msg00017.html](https://lists.debian.org/debian-hurd/2017/06/msg00017.html)
