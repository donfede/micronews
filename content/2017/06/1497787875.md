Title: Debian would like to thank the upstream developers of all the software in Debian stretch https://packages.debian.org/ #releasingstretch
Slug: 1497787875
Date: 2017-06-18 12:11
Author: Paul Wise
Status: published

Debian would like to thank the upstream developers of all the software in Debian stretch [https://packages.debian.org/](https://packages.debian.org/) #releasingstretch
