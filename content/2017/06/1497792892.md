Title: The publicity team has provided live coverage of every Debian release from squeeze to stretch! Hope you enjoyed it! #releasingstretch
Slug: 1497792892
Date: 2017-06-18 13:34
Author: Paul Wise
Status: published

The publicity team has provided live coverage of every Debian release from squeeze to stretch! Hope you enjoyed it! #releasingstretch
