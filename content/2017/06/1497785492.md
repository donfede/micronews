Title: Debian would like to thank our sponsors of about 50 primary mirrors and 413 total mirrors https://www.debian.org/mirror/sponsors #releasingstretch
Slug: 1497785492
Date: 2017-06-18 11:31
Author: Paul Wise
Status: published

Debian would like to thank our sponsors of about 50 primary mirrors and 413 total mirrors [https://www.debian.org/mirror/sponsors](https://www.debian.org/mirror/sponsors) #releasingstretch
