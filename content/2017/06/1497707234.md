Title: Support channels for Debian: forums, mailing lists, the bug tracker, chat, consultants... https://www.debian.org/support #releasingstretch
Slug: 1497707234
Date: 2017-06-17 13:47
Author: Laura Arjona Reina
Status: published

Support channels for Debian: forums, mailing lists, the bug tracker, chat, consultants... [https://www.debian.org/support](https://www.debian.org/support) #releasingstretch
