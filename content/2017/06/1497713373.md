Title: Also there is a website theme based on softWaves, made by Elisa Godoy https://gitlab.com/yemanjalisa/debianstretchwebsite Thanks! #releasingstretch
Slug: 1497713373
Date: 2017-06-17 15:29
Author: Laura Arjona Reina
Status: published

Also there is a website theme based on softWaves, made by Elisa Godoy [https://gitlab.com/yemanjalisa/debianstretchwebsite](https://gitlab.com/yemanjalisa/debianstretchwebsite) Thanks! #releasingstretch
