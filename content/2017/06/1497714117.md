Title: Stretch provides the LIO SCSI stack, with many fabric modules: FC, FCoE, iSCSI, iSER, tcm_loop etc. #releasingstretch
Slug: 1497714117
Date: 2017-06-17 15:41
Author: Paul Wise
Status: published

Stretch provides the LIO SCSI stack, with many fabric modules: FC, FCoE, iSCSI, iSER, tcm_loop etc. #releasingstretch
