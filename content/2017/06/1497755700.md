Title: If any of the lines in your /etc/apt/sources.list refer to 'stable', you might get a surprise on your next upgrade! #releasingstretch 
Slug: 1497755700
Date: 2017-06-18 03:15
Author: Donald Norwood
Status: published

If any of the lines in your /etc/apt/sources.list refer to 'stable', you might get a surprise on your next upgrade! #releasingstretch 
