Title: Debian-Installer 'Stretch' Alpha 8 release https://lists.debian.org/debian-devel-announce/2016/11/msg00006.html
Slug: 1479151407
Date: 2016-11-14 19:23
Author: Ana Guerrero Lopez
Status: published

Debian-Installer 'Stretch' Alpha 8 release [https://lists.debian.org/debian-devel-announce/2016/11/msg00006.html](https://lists.debian.org/debian-devel-announce/2016/11/msg00006.html)
