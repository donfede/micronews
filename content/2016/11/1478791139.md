Title: Debian will be present at T-DOSE 11th edition, Eindhoven, Netherlands, Nov 12-13 https://lists.debian.org/debian-events-nl/2016/08/msg00002.html
Slug: 1478791139
Date: 2016-11-10 15:18
Author: Jean-Pierre Giraud
Status: published

Debian will be present at T-DOSE 11th edition, Eindhoven, Netherlands, Nov 12-13 [https://lists.debian.org/debian-events-nl/2016/08/msg00002.html](https://lists.debian.org/debian-events-nl/2016/08/msg00002.html)
