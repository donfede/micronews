Title: DebConf16 sponsored registration deadline approaching: 10 April 2016
Slug: 1460053926
Date: 2016-04-07 18:32:06
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/zzbQ5XEdTUmmqJPyIxdtcQ

Deadline for Debconf16 sponsorship applications is this Sunday 10 April 2016 <a href="https://lists.debian.org/debian-devel-announce/2016/04/msg00001.html">https://lists.debian.org/debian-devel-announce/2016/04/msg00001.html</a>

