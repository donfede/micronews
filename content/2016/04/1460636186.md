Title: Let's welcome Hetzner as a Silver Sponsor for DebConf16!
Slug: 1460636186
Date: 2016-04-14 12:16:26
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/IwbmP7E5ThWPCWpOmqnuvg

Let's welcome <a href="https://hetzner.co.za/">Hetzner</a> as a Silver Sponsor for DebConf16! Do you want to <a href="http://debconf16.debconf.org/">sponsor</a> too?
