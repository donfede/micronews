Title: News from Debian: Updated Debian 8: 8.4 released
Slug: 1459609804
Date: 2016-04-02 15:10:04
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/9HQJqbo7TWqOv2MUL82B4w

News from Debian: <a href="https://www.debian.org/News/2016/20160402">Updated Debian 8: 8.4 released</a>

