Title: Let's welcome Takealot.com as a Silver Sponsor for DebConf16!
Slug: 1466072688
Date: 2016-06-16 10:24:48
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/rjHEqUZzRvCiqNoXWiK-TA

Let's welcome <a href="http://www.takealot.com/">Takealot.com</a> as a Silver Sponsor for DebConf16! Do you want to <a href="http://debconf16.debconf.org/">sponsor</a> too?
