Title: Let's welcome Plat'Home as a Bronze Sponsor for DebConf16!
Slug: 1465474982
Date: 2016-06-09 12:23:02
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/yLCAk43cSCuT9nIPVryEIA

Let's welcome <a href="http://www.plathome.com/">Plat'Home</a> as a Bronze Sponsor for DebConf16! Do you want to <a href="http://debconf16.debconf.org/">sponsor</a> too?
