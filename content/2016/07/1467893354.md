Title: DebConf16 events for Thursday 7 July (early afternoon)
Slug: 1467893354
Date: 2016-07-07 12:09:14
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/ploZ95hpSomQcqRBsLM9Cg

DebConf16 events this early afternoon: <ul><li><a href="https://debconf16.debconf.org/talks/34/">The Conservancy Debian Services Agreement: One Year Hence </a>by Bradley Kuhn </li> <li><a href="https://debconf16.debconf.org/talks/4/">What's new in the Linux kernel</a> by Ben Hutchings </li> <li><a href="https://debconf16.debconf.org/talks/134/">Debian from 10,000 feet BOF</a> by Mehdi Dogguy &amp; Lucas Nussbaum </li> <li><a href="https://debconf16.debconf.org/talks/137/">KERN - the radio astronomy software suite used at SKA, based on Ubuntu</a> by gijs molenaar </li> <li><a href="https://debconf16.debconf.org/talks/17/">autopkgtest skills exchange</a> by Antonio Terceiro </li> <li><a href="https://debconf16.debconf.org/talks/105/">Lightning talks</a> by Nattie Mayer-Hutchings  </li> <li><a href="https://debconf16.debconf.org/talks/50/">LTTng: Kernel and userspace tracing in Debian</a> by Michael Jeanson  </li> <li><a href="https://debconf16.debconf.org/talks/128/">Bursaries BOF</a> by David Bremner  </li> <li><a href="https://debconf16.debconf.org/talks/10/">Using LAVA for Debian</a> by Neil Williams </li> <li><a href="https://debconf16.debconf.org/talks/127/">Installing dracut on your computer</a> by Thomas Lange  </li></ul> <br />Join us if you are in Cape Town! </p> Or follow the live streaming at <a href="https://debconf16.debconf.org/video/live-streaming/">https://debconf16.debconf.org/video/live-streaming/</a> </p>  </p>

