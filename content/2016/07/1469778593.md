Title: Debconf video recordings broadcasted on Frikanalen (Norwegian TV)
Slug: 1469778593
Date: 2016-07-29 07:49:53
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/6bmihJ30RG2tD9GLymS6Zg

Debconf video recordings were broadcasted last week on <a href="http://beta.frikanalen.no/">Frikanalen</a>,  a Norwegian open TV channel with national coverage.</p>

