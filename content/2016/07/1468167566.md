Title: Help subtitling the DebConf16 videos!
Slug: 1468167566
Date: 2016-07-10 16:19:26
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/eFpKN4RtTsS_WV_qm3y90g

Now that you will be <a href="http://meetings-archive.debian.net/pub/debian-meetings/2016/debconf16/">downloading and watching DebConf16 videos</a> for a while, what about transcribing them (and maybe translating)? Join the <a href="https://lists.debconf.org/lurker/message/20160708.191102.8c361911.en.html">DebConf Subtitle taskforce</a>!</p>

