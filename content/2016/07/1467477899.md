Title: DebConf16 events for Saturday 2 July (afternoon)
Slug: 1467477899
Date: 2016-07-02 16:44:59
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/osw18qd5TMGkfcDpFulAHA

<p>DebConf16 attendants were welcomed today with the <a href="https://debconf16.debconf.org/talks/116/">&ldquo;New to DebConf BoF&rdquo;</a> by Bernelle Verster and Rhonda D'Vine. The DebConf16 Open Festival goes on tomorrow. Join us in person or remotely!


