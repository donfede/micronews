Title: DebConf16 events for Tuesday 5 July (morning)
Slug: 1467705943
Date: 2016-07-05 08:05:43
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/nEFHZSKaTnq2L-KUD9cQ1g

DebConf16 events this morning: <ul><li><a href="https://debconf16.debconf.org/talks/51/">Bits from the DPL</a> by Mehdi Dogguy  </li> <li><a href="https://debconf16.debconf.org/talks/80/">Putting GNU/kFreeBSD into production</a> by Steven Chamberlain  </li> <li><a href="https://debconf16.debconf.org/talks/125/">Debian policy BoF</a> by Andi Barth</li> <li><a href="https://debconf16.debconf.org/talks/1/">dbconfig-common: taking it the next mile</a> by Paul Gevers   (continues with BoF)</li> <li><a href="https://debconf16.debconf.org/talks/129/">Fundraising BoF</a> by Michael Banck </li> <li><a href="https://debconf16.debconf.org/talks/89/">systemd in Debian - a status update</a> by Michael Biebl </li></ul> <p style=" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Join us if you are in Cape Town! </p> Or follow the live streaming at <a href="https://debconf16.debconf.org/video/live-streaming/">https://debconf16.debconf.org/video/live-streaming/</a>   </p>  </p> <br /></p>

