Title: DebConf16 events for Saturday 2 July (morning)
Slug: 1467445405
Date: 2016-07-02 07:43:25
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/aYsjhODwTgWLRLJX3-DL-g

DebConf16 events for this morning:  <ul><li><a href="https://debconf16.debconf.org/talks/119/">&quot;Debugging the IoT - Open Hardware Panel&quot;</a> by Bernelle Verster, et al. </li> <li><a href="https://debconf16.debconf.org/talks/118/">&quot;FLOSSing for healthier society&quot;</a>, by jbothma</li> <li><a href="https://debconf16.debconf.org/talks/60/">&quot;My Experience with Debian&quot;</a> by Shirish Agarwal</li></ul> <br /></p> Join us if you are in Cape Town! Or follow the live streaming at <a href="https://debconf16.debconf.org/video/live-streaming/">https://debconf16.debconf.org/video/live-streaming/</a> </p>

