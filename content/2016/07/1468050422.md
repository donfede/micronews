Title: DebConf17 will take place in Montreal, Canada, 6 to 12 August 2017
Slug: 1468050422
Date: 2016-07-09 07:47:02
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/2xPgG--8Q1Kj3tNDul0ZnA

DebConf17 dates announced: <ul><li>DebCamp starts 2017-07-31 </li> <li>DebianDay on 2017-08-05 </li> <li>DebConf 2017-08-06 to 2017-08-12</li></ul> <br />See you next year in Montreal, Canada!</p>

