Title: Survey for the default artwork for Stretch https://lists.debian.org/debian-desktop/2016/10/msg00000.html
Slug: 1475679039
Date: 2016-10-05 14:50
Author: Laura Arjona Reina
Status: published

Survey for the default artwork for Stretch [https://lists.debian.org/debian-desktop/2016/10/msg00000.html](https://lists.debian.org/debian-desktop/2016/10/msg00000.html)
