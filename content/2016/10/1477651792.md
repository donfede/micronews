Title: Laura Arjona Reina: Rankings, Condorcet and free software: Calculating the results for the Stretch Artwork Survey
Slug: 1477651792
Date: 2016-10-28 10:49
Author: Ana Guerrero Lopez
Status: published

Laura Arjona Reina: "Rankings, Condorcet and free software: Calculating the results for the Stretch Artwork Survey" [https://larjona.wordpress.com/2016/10/25/rankings-condorcet-and-free-software-calculating-the-results-for-the-stretch-artwork-survey/](https://larjona.wordpress.com/2016/10/25/rankings-condorcet-and-free-software-calculating-the-results-for-the-stretch-artwork-survey/)
