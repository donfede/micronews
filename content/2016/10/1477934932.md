Title: Stretch will be released for 10 architectures. Changes from Jessie: removal of powerpc and adding of mips64el.
Slug: 1477934932
Date: 2016-10-31 17:28
Author: Ana Guerrero Lopez
Status: published

Stretch will be released for 10 architectures. Changes from Jessie: removal of powerpc and adding of mips64el. [https://lists.debian.org/debian-devel-announce/2016/10/msg00008.html](https://lists.debian.org/debian-devel-announce/2016/10/msg00008.html)
