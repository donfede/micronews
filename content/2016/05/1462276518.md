Title: Our third DebConf16 Bronze Sponsor is Bluemosh
Slug: 1462276518
Date: 2016-05-03 11:55:18
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/PTB8ebVLTR6pr9oiKzF6sQ

Our third DebConf16 Bronze Sponsor is Bluemosh. Thanks <a href="https://bluemosh.com/">Bluemosh</a>! Do you want to <a href="https://debconf16.debconf.org">sponsor</a> too?
