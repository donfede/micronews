Title: The Debian Project mourns the loss of Kristoffer H. Rose https://www.debian.org/News/2016/20160927
Slug: 1475020500
Date: 2016-09-27 23:55
Author: Ana Guerrero López
Status: published

The Debian Project mourns the loss of Kristoffer H. Rose [https://www.debian.org/News/2016/20160927](https://www.debian.org/News/2016/20160927)
