Title: DebConf16 photos (high resolution) available
Slug: 1470422020
Date: 2016-08-05 18:33:40
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/NnraA2dCTEKg9M_WMyQAdQ

The DebConf16 group photos (high resolution) are available at <a href="https://gallery.debconf.org/main.php?g2_itemId=62958">https://gallery.debconf.org/main.php?g2_itemId=62958</a> 

