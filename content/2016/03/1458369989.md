Title: Registration is now open for DebConf16!
Slug: 1458369989
Date: 2016-03-19 06:46:29
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/whD420R1QGa5XHk1XipZZg

<a href="https://debconf16.debconf.org">Debconf 16</a> will take place in Cape Town, South Africa, from Saturday, 2 July to Saturday, 9 July 2016. It will be preceded by 9 days of DebCamp, from Thursday,</p> 23 June to Friday, 1 June 2016.</p> <br /></p> <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Registration is now open for DebConf16. Please visit the website, log in,</p> <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">and register your attendance. More details in the <a href="https://lists.debian.org/debian-devel-announce/2016/03/msg00008.html">announcement</a>.</p> <p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p> <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">We look forward to seeing you in Cape Town In July!</p>

