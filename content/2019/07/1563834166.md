Title: Meanwhile, Debian webmasters and interested people will have a "Web site old content cleanup work session" - no video streaming, but you can follow the activity anytime at https://salsa.debian.org/webmaster-team/webwml/activity ;)
Slug: 1563834166
Date: 2019-07-22 22:22
Author: Donald Norwood
Status: published

Meanwhile, Debian webmasters and interested people will have a "Web site old content cleanup work session" - no video streaming, but you can follow the activity anytime at [https://salsa.debian.org/webmaster-team/webwml/activity](https://salsa.debian.org/webmaster-team/webwml/activity) ;)
