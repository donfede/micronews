Title: Meanwhile, a 2h workshop about Debian packaging will happen in room A102: "Empacotamento de software no Debian: uma visão geral e introdutória do processo" https://debconf19.debconf.org/talks/166-empacotamento-de-software-no-debian-uma-visao-geral-e-introdutoria-do-processo/
Slug: 1564152623
Date: 2019-07-26 14:50
Author: Laura Arjona Reina
Status: published

Meanwhile, a 2h workshop about Debian packaging will happen in room A102: "Empacotamento de software no Debian: uma visão geral e introdutória do processo" [https://debconf19.debconf.org/talks/166-empacotamento-de-software-no-debian-uma-visao-geral-e-introdutoria-do-processo/](https://debconf19.debconf.org/talks/166-empacotamento-de-software-no-debian-uma-visao-geral-e-introdutoria-do-processo/)
