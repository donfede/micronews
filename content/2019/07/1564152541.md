Title: We start day 6 of the Conference with a single talk: "Free Software/Utopia" #Debconf19 - follow the live streaming in https://debconf19.debconf.org/schedule/venue/1/
Slug: 1564152541
Date: 2019-07-26 14:49
Author: Laura Arjona Reina
Status: published

We start day 6 of the Conference with a single talk: "Free Software/Utopia" #Debconf19 - follow the live streaming in [https://debconf19.debconf.org/schedule/venue/1/](https://debconf19.debconf.org/schedule/venue/1/)
