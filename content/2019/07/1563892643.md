Title: Third day of #DebConf19 in Curitiba! See the schedule for today at https://debconf19.debconf.org/schedule/?day=2019-07-23
Slug: 1563892643
Date: 2019-07-23 14:37
Author: Laura Arjona Reina
Status: published

Third day of #DebConf19 in Curitiba! See the schedule for today at [https://debconf19.debconf.org/schedule/?day=2019-07-23](https://debconf19.debconf.org/schedule/?day=2019-07-23)
