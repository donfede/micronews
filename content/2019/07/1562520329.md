Title: If you use apt-get to upgrade to buster, you won't be asked about the security repository info changed from 'testing' to 'stable'. Use the --allow-releaseinfo-change option, as Eriberto Mota explains here: http://eriberto.pro.br/blog/2019/07/07/debian-repository-changed-its-suite-value-from-testing-to-stable/ or use apt instead
Slug: 1562520329
Date: 2019-07-07 17:25
Author: Laura Arjona Reina
Status: published

If you use apt-get to upgrade to buster, you won't be asked about the security repository info changed from 'testing' to 'stable'. Use the --allow-releaseinfo-change option, as Eriberto Mota explains here: [http://eriberto.pro.br/blog/2019/07/07/debian-repository-changed-its-suite-value-from-testing-to-stable/](http://eriberto.pro.br/blog/2019/07/07/debian-repository-changed-its-suite-value-from-testing-to-stable/) or use apt instead
