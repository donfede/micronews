Title: Wondering where #DebConf21 will be? Explore some available destinations following the "DebConf21: in  ... your city?" session held in the Auditório https://debconf19.debconf.org/schedule/venue/1/ #DebConf19
Slug: 1564255183
Date: 2019-07-27 19:19
Author: Donald Norwood
Status: published

Wondering where #DebConf21 will be? Explore some available destinations following the "DebConf21: in  ... your city?" session held in the Auditório [https://debconf19.debconf.org/schedule/venue/1/](https://debconf19.debconf.org/schedule/venue/1/) #DebConf19
