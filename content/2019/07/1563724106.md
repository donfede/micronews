Title: After the Opening Ceremony, #DebConf19 continues today with "Debian in the Cloud", "Symbolic Execution of Maintainer Scripts" and "R packaging BoF" https://debconf19.debconf.org/schedule/?day=2019-07-21
Slug: 1563724106
Date: 2019-07-21 15:48
Author: Laura Arjona Reina
Status: published

After the Opening Ceremony, #DebConf19 continues today with "Debian in the Cloud", "Symbolic Execution of Maintainer Scripts" and "R packaging BoF" [https://debconf19.debconf.org/schedule/?day=2019-07-21](https://debconf19.debconf.org/schedule/?day=2019-07-21)
