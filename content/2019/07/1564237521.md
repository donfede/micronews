Title: "A new home for Debian in the Mastodon / ActivityPub fediverse: follow @debian@framapiaf.org (and possible future moves)" by Laura Arjona Reina https://larjona.wordpress.com/2019/07/27/a-new-home-for-debian-in-the-mastodon-activitypub-fediverse-follow-debianframapiaf-org-and-possible-future-moves/
Slug: 1564237521
Date: 2019-07-27 14:25
Author: Laura Arjona Reina
Status: published

"A new home for Debian in the Mastodon / ActivityPub fediverse: follow @debian@framapiaf.org (and possible future moves)" by Laura Arjona Reina [https://larjona.wordpress.com/2019/07/27/a-new-home-for-debian-in-the-mastodon-activitypub-fediverse-follow-debianframapiaf-org-and-possible-future-moves/](https://larjona.wordpress.com/2019/07/27/a-new-home-for-debian-in-the-mastodon-activitypub-fediverse-follow-debianframapiaf-org-and-possible-future-moves/)
