Title: Debian buster is the first release with a debian-policy mandating 'packages should be reproducible' (since policy 4.1.0) #ReleasingDebianBuster
Slug: 1562445091
Date: 2019-07-06 20:31
Author: Donald Norwood
Status: published

Debian buster is the first release with a debian-policy mandating 'packages should be reproducible' (since policy 4.1.0) #ReleasingDebianBuster
