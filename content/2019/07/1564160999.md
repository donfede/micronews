Title: Debian feeds freedom of software, but our Developers still have to eat lunch, so we pause for this non-commercial break. Non-Commercial, get it? #DebianIsHilarious. #VagueJoke #Debconf19
Slug: 1564160999
Date: 2019-07-26 17:09
Author: Donald Norwood
Status: published

Debian feeds freedom of software, but our Developers still have to eat lunch, so we pause for this non-commercial break. Non-Commercial, get it? #DebianIsHilarious. #VagueJoke #DebConf19

![Conference lunch break #DebConf19](|static|/images/DebConf19OfflineLunch.jpg)
