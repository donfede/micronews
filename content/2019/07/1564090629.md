Title: Last events for the day follow: "Electronic Experimental Music with Debian", "Building a Debian derivative from Git", and "The Free Software Foundation and Debian" #Debconf19 https://debconf19.debconf.org/schedule/?day=2019-07-25
Slug: 1564090629
Date: 2019-07-25 21:37
Author: Donald Norwood
Status: published

Last events for the day follow: "Electronic Experimental Music with Debian", "Building a Debian derivative from Git", and "The Free Software Foundation and Debian" #Debconf19 [https://debconf19.debconf.org/schedule/?day=2019-07-25](https://debconf19.debconf.org/schedule/?day=2019-07-25)
