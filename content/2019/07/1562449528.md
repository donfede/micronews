Title: GNOME in Debian 10 buster has changed its default display server from Xorg to Wayland. https://wiki.debian.org/Wayland #ReleasingDebianBuster
Slug: 1562449528
Date: 2019-07-06 21:45
Author: Donald Norwood
Status: published

GNOME in Debian 10 buster has changed its default display server from Xorg to Wayland. [https://wiki.debian.org/Wayland](https://wiki.debian.org/Wayland) #ReleasingDebianBuster
