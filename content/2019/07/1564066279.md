Title: This morning #DebConf19 talks start fresh with: "E: lintian: bits-from-the-lintian-maintainers", "Building Medical Devices with Debian", and a BoF about Geographical Diversity https://debconf19.debconf.org/schedule/?day=2019-07-25
Slug: 1564066279
Date: 2019-07-25 14:51
Author: Donald Norwood
Status: published


This morning #DebConf19 talks start fresh with: "E: lintian: bits-from-the-lintian-maintainers", "Building Medical Devices with Debian", and a BoF about Geographical Diversity [https://debconf19.debconf.org/schedule/?day=2019-07-25](https://debconf19.debconf.org/schedule/?day=2019-07-25)
