Title: Last set of talks at #DebConf19 Open Day will start in a few minutes: "Who's afraid of Spectre and Meltdown?", "Benefícios de uma comunidade local de contribuidores FLOSS" and "Debian na vida de uma Operadora de Telecom", followed by "New to DebConf BoF", "Comunidades: o bom, o ruim e o maravilhoso" and "Automação total desde a criação da imagem de instalação ao ambiente de produção em um Grande Banco Público"  https://debconf19.debconf.org/schedule/?day=2019-07-20
Slug: 1563655848
Date: 2019-07-20 20:50
Author: Laura Arjona Reina
Status: published

Last set of talks at #DebConf19 Open Day will start in a few minutes: "Who's afraid of Spectre and Meltdown?", "Benefícios de uma comunidade local de contribuidores FLOSS" and "Debian na vida de uma Operadora de Telecom", followed by "New to DebConf BoF", "Comunidades: o bom, o ruim e o maravilhoso" and "Automação total desde a criação da imagem de instalação ao ambiente de produção em um Grande Banco Público"  [https://debconf19.debconf.org/schedule/?day=2019-07-20](https://debconf19.debconf.org/schedule/?day=2019-07-20)
