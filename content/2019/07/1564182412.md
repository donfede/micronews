Title: One of the options for DebConf19 attendants for this evening is "Movie screening: The Edge of Democracy (2019)" https://debconf19.debconf.org/talks/171-movie-screening-the-edge-of-democracy-2019/
Slug: 1564182412
Date: 2019-07-26 23:06
Author: Donald Norwood
Status: published

One of the options for DebConf19 attendants for this evening is "Movie screening: The Edge of Democracy (2019)" [https://debconf19.debconf.org/talks/171-movie-screening-the-edge-of-democracy-2019/](https://debconf19.debconf.org/talks/171-movie-screening-the-edge-of-democracy-2019/)
