Title: Good morning from Curitiba! See the schedule for today at #DebConf19 in https://debconf19.debconf.org/schedule/?day=2019-07-27
Slug: 1564239457
Date: 2019-07-27 14:57
Author: Laura Arjona Reina
Status: published

Good morning from Curitiba! See the schedule for today at #DebConf19 in [https://debconf19.debconf.org/schedule/?day=2019-07-27](https://debconf19.debconf.org/schedule/?day=2019-07-27)
