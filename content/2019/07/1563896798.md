Title: "When Antitrust Law Fails: Breaking Up Big Tech with Grassroots Technology", "How to decrease Debian font bugs by decreasing Debian fonts", and "Debian Go packaging team BoF" in a few minutes in #DebConf19  https://debconf19.debconf.org/schedule/?day=2019-07-23
Slug: 1563896798
Date: 2019-07-23 15:46
Author: Donald Norwood
Status: published

"When Antitrust Law Fails: Breaking Up Big Tech with Grassroots Technology", "How to decrease Debian font bugs by decreasing Debian fonts", and "Debian Go packaging team BoF" in a few minutes in #DebConf19  [https://debconf19.debconf.org/schedule/?day=2019-07-23](https://debconf19.debconf.org/schedule/?day=2019-07-23)
