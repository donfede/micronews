Title: Last Friday of July! Today is the System Administrator Appreciation Day. In Debian we're very proud and thankful to the DSA Team https://dsa.debian.org/ You rock! #SysAdminDay
Slug: 1564152073
Date: 2019-07-26 14:41
Author: Laura Arjona Reina
Status: published

Last Friday of July! Today is the System Administrator Appreciation Day. In Debian we're very proud and thankful to the DSA Team [https://dsa.debian.org/](https://dsa.debian.org/) You rock! #SysAdminDay
