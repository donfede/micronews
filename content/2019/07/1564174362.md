Title: The afternoon continues with "DJCLI: Case Study in how Debian Opens Opportunities for those with Different Needs", "Is Debian (and Free Software) gender diverse enough?", and the Web team BoF #Debconf19 https://debconf19.debconf.org/schedule/?day=2019-07-26
Slug: 1564174362
Date: 2019-07-26 20:52
Author: Donald Norwood
Status: published

The afternoon continues with "DJCLI: Case Study in how Debian Opens Opportunities for those with Different Needs", "Is Debian (and Free Software) gender diverse enough?", and the Web team BoF. 2 additional talks added which are unfortunately not streamed (sorry): Outreach BoF and "Debian in the Embedded World" #DebConf19 [https://debconf19.debconf.org/schedule/?day=2019-07-26](https://debconf19.debconf.org/schedule/?day=2019-07-26)
