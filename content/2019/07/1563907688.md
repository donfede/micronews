Title: "Debian Games: Fun and Freedom - What is new? - Ask your questions and learn more!", "How MariaDB packaging uses Salsa-CI to ensure smooth upgrades and avoid regressions", and the Ruby Team BOF now at #DebConf19
Slug: 1563907688
Date: 2019-07-23 18:48
Author: Donald Norwood
Status: published

"Debian Games: Fun and Freedom - What is new? - Ask your questions and learn more!", "How MariaDB packaging uses Salsa-CI to ensure smooth upgrades and avoid regressions", and the Ruby Team BOF now at #DebConf19 [https://debconf19.debconf.org/schedule/?day=2019-07-23](https://debconf19.debconf.org/schedule/?day=2019-07-23)
