Title: Next event: "State of the Debian Keyring", short talk held in the Auditorium #Debconf19 https://debconf19.debconf.org/schedule/venue/1/
Slug: 1564071793
Date: 2019-07-25 16:23
Author: Laura Arjona Reina
Status: published

Next event: "State of the Debian Keyring", short talk held in the Auditorium #Debconf19 [https://debconf19.debconf.org/schedule/venue/1/](https://debconf19.debconf.org/schedule/venue/1/)
