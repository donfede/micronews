Title: Firefox cert expired, disabling all the addons is fixed in firefox 66.0.4 (sid) and firefox-esr 60.6.2 (buster, stretch and jessie)
Slug: 1557259654
Date: 2019-05-07 20:07
Author: Ana Guerrero Lopez
Status: published

Firefox cert expired, disabling all the addons is fixed in firefox 66.0.4 (sid) and firefox-esr 60.6.2 (buster, stretch and jessie)
