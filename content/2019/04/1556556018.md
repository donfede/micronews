Title: DebConf 19 CFP deadline extension, and content team office hours https://lists.debian.org/debian-devel-announce/2019/04/msg00009.html #DebConf19
Slug: 1556556018
Date: 2019-04-29 16:40
Author: Laura Arjona Reina
Status: published

DebConf 19 CFP deadline extension, and content team office hours [https://lists.debian.org/debian-devel-announce/2019/04/msg00009.html](https://lists.debian.org/debian-devel-announce/2019/04/msg00009.html) #DebConf19
