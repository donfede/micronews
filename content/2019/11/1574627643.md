Title: There will be a Debian booth in Paris Open Source Summit (@OSS_Paris) near from Paris on 10th and 11th December 2019 with @DebianFrance team. Come to greet us! https://www.opensourcesummit.paris/
Slug: 1574627643
Date: 2019-11-24 20:34
Author: Alban Vidal
Status: published

There will be a Debian booth in Paris Open Source Summit (@OSS_Paris) near from Paris on 10th and 11th December 2019 with @DebianFrance team. Come to greet us! [https://www.opensourcesummit.paris/](https://www.opensourcesummit.paris/)
