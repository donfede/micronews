Title: Bits from @lolamby, the @Debian Project Leader (March 2019) -- https://lists.debian.org/debian-devel-announce/2019/03/msg00012.html
Slug: 1554051156
Date: 2019-03-31 16:52
Author: Chris Lamb
Status: published

Bits from @lolamby, the @Debian Project Leader (March 2019) -- [https://lists.debian.org/debian-devel-announce/2019/03/msg00012.html](https://lists.debian.org/debian-devel-announce/2019/03/msg00012.html)
