Title: This weekend we have a #Debian Bug Squashing #Party in Cambridge, UK https://wiki.debian.org/BSP/2019/03/gb/Cambridge #BSP
Slug: 1552047332
Date: 2019-03-08 12:15
Author: Laura Arjona Reina
Status: published

This weekend we have a #Debian Bug Squashing #Party in Cambridge, UK [https://wiki.debian.org/BSP/2019/03/gb/Cambridge](https://wiki.debian.org/BSP/2019/03/gb/Cambridge) #BSP
