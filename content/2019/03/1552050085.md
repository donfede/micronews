Title: The Debian Project mourns the loss of Lucy Wayland https://www.debian.org/News/2019/20190308
Slug: 1552050085
Date: 2019-03-08 13:01
Author: Laura Arjona Reina
Status: published

The Debian Project mourns the loss of Lucy Wayland [https://www.debian.org/News/2019/20190308](https://www.debian.org/News/2019/20190308)
