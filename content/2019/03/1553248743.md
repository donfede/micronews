Title: Removal of #Wheezy and #Jessie (except #LTS) from mirrors https://lists.debian.org/debian-devel-announce/2019/03/msg00006.html
Slug: 1553248743
Date: 2019-03-22 09:59
Author: Laura Arjona Reina
Status: published

Removal of #Wheezy and #Jessie (except #LTS) from mirrors [https://lists.debian.org/debian-devel-announce/2019/03/msg00006.html](https://lists.debian.org/debian-devel-announce/2019/03/msg00006.html)
