Title: What to expect in Debian 11 Bullseye for nftables/iptables https://ral-arturo.org/2019/10/14/debian-netfilter.html
Slug: 1571139833
Date: 2019-10-15 11:43
Author: Arturo Borrero Gonzalez
Status: published

What to expect in Debian 11 Bullseye for nftables/iptables [https://ral-arturo.org/2019/10/14/debian-netfilter.html](https://ral-arturo.org/2019/10/14/debian-netfilter.html)
