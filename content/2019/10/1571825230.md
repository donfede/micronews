Title: The Debian Project stands with the @GNOME Foundation in defense against patent trolls https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll.html
Slug: 1571825230
Date: 2019-10-23 10:07
Author: Laura Arjona Reina
Status: published

The Debian Project stands with the @GNOME Foundation in defense against patent trolls [https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll.html](https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll.html)
