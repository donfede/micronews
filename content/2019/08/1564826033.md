Title: New Debian Developers and Maintainers (May and June 2019) https://bits.debian.org/2019/08/new-developers-2019-06.html
Slug: 1564826033
Date: 2019-08-03 09:53
Author: Ana Guerrero Lopez
Status: published

New Debian Developers and Maintainers (May and June 2019) [https://bits.debian.org/2019/08/new-developers-2019-06.html](https://bits.debian.org/2019/08/new-developers-2019-06.html)
