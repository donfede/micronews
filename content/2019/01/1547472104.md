Title: "futurePrototype" will be the default theme for Debian 10 https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html
Slug: 1547472104
Date: 2019-01-14 13:21
Author: Laura Arjona Reina
Status: published

"futurePrototype" will be the default theme for Debian 10 [https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html](https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html)
