Title: Distribution roundtable discussion with the @Debian, @elementary and @Fedora Project Leaders -- https://www.linuxjournal.com/content/state-desktop-linux-2019
Slug: 1546359322
Date: 2019-01-01 16:15
Author: Chris Lamb
Status: published

Distribution roundtable discussion with the @Debian, @elementary and @Fedora Project Leaders -- [https://www.linuxjournal.com/content/state-desktop-linux-2019](https://www.linuxjournal.com/content/state-desktop-linux-2019)
