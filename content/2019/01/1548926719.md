Title: Bits from the @Debian Project Leader (January 2019) -- https://lists.debian.org/debian-devel-announce/2019/01/msg00010.html
Slug: 1548926719
Date: 2019-01-31 09:25
Author: Chris Lamb
Status: published

Bits from the @Debian Project Leader (January 2019) -- [https://lists.debian.org/debian-devel-announce/2019/01/msg00010.html](https://lists.debian.org/debian-devel-announce/2019/01/msg00010.html)
