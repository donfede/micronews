Title: Bits from the @Debian Project Leader (February 2019) -- https://lists.debian.org/debian-devel-announce/2019/02/msg00010.html
Slug: 1551379442
Date: 2019-02-28 18:44
Author: Chris Lamb
Status: published

Bits from the @Debian Project Leader (February 2019) -- [https://lists.debian.org/debian-devel-announce/2019/02/msg00010.html](https://lists.debian.org/debian-devel-announce/2019/02/msg00010.html)
