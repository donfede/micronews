Title: None
Slug: 1399064334
Date: 2014-05-02 20:58:54
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/if0t4fELT86RDRKgfdgB6g

the Debian KDE/Qt packaging team is looking for help with bugs, patches, packaging and docs - <a href="https://lists.debian.org/20140501181922.GD14582@gnuservers.com.ar">join today!</a>

