Title: DebConf14 final report
Slug: 1416094517
Date: 2014-11-15 23:35:17
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/m-8AdA1iS66R3YvGnaj4vA

the DebConf team have released the <a href="http://media.debconf.org/dc14/report/DebConf14_final_report.en.pdf">final report on DebConf14</a> [PDF] in Portland, OR, USA!

