Title: Help the Debian publicity team
Slug: 1416094850
Date: 2014-11-15 23:40:50
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/cgBOgWlYQNWNMgvzBp4dyQ

Learn how to <a href="https://lists.debian.org/20141114115824.GA4049@pryan.ekaia.org">help out</a> the <a href="https://wiki.debian.org/Teams/Publicity">Debian publicity team</a> and highlight the cool things happening in Debian!

