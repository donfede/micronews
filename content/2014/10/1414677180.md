Title: DebConf15 has a third silver sponsor
Slug: 1414677180
Date: 2014-10-30 13:53:00
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/kdZwkpEuR3y-hUXNhzy07g

The 3rd DebConf15 silver sponsor is <a href="http://martin-alfke.de">@tuxmea</a>: Thank you so much! Want to <a href="http://debconf15.debconf.org/become-sponsor.xhtml">sponsor</a> #DebConf15 too?

