Title: More DebConf15 sponsorship needed!
Slug: 1413808844
Date: 2014-10-20 12:40:44
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/A6iGeXtGQiuLnUkqoMnaiQ

<a href="http://debconf15.debconf.org/">DebConf15</a> has a first Bronze sponsor: <a href="http://www.logilab.fr/">Logilab</a>. Who's <a href="http://debconf15.debconf.org/become-sponsor.xhtml">next</a>?

