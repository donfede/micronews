Title: None
Slug: 1408908790
Date: 2014-08-24 19:33:10
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/x4iUZKo9TouL1uiwOGgvtA

Debian will participate in the <a href="https://gnome.org/opw/">Outreach Program for Women</a>, and is <a href="https://lists.debian.org/20140824172451.GB22299@xanadu.blop.info">looking for sponsors, mentors and organisers</a>.

