Title: None
Slug: 1398655470
Date: 2014-04-28 03:24:30
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/wGTQKNRURbyIPZYTrb8HBA

Debian sparc port <a href="https://lists.debian.org/20140426213456.GA12541@spike.0x539.de">removed</a> from jessie due to lack of porters, toolchain problems and machine stability :(

