Title: "Brief update about software freedom and artificial intelligence" by M. Zhou https://lists.debian.org/debian-project/2023/02/msg00017.html
Slug: 1677270982
Date: 2023-02-24 20:36
Author: Laura Arjona Reina
Status: published

"Brief update about software freedom and artificial intelligence" by M. Zhou [https://lists.debian.org/debian-project/2023/02/msg00017.html](https://lists.debian.org/debian-project/2023/02/msg00017.html)
