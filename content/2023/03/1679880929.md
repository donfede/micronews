Title: Debian 9 stretch, stretch-debug and stretch-proposed-updates suites moved to archive.debian.org https://lists.debian.org/debian-devel-announce/2023/03/msg00006.html
Slug: 1679880929
Date: 2023-03-27 01:35
Author: Jean-Pierre Giraud
Status: published

Debian 9 stretch, stretch-debug and stretch-proposed-updates suites moved to archive.debian.org [https://lists.debian.org/debian-devel-announce/2023/03/msg00006.html](https://lists.debian.org/debian-devel-announce/2023/03/msg00006.html)
