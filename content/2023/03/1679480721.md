Title: Debian Reunion Hamburg 2023 from May 23 to 30 https://lists.debian.org/debian-devel-announce/2023/03/msg00005.html
Slug: 1679480721
Date: 2023-03-22 10:25
Author: Laura Arjona Reina
Status: published

Debian Reunion Hamburg 2023 from May 23 to 30 [https://lists.debian.org/debian-devel-announce/2023/03/msg00005.html](https://lists.debian.org/debian-devel-announce/2023/03/msg00005.html)
