Title: Survey to gather feedback on the native Debian package of GitLab https://lists.debian.org/debian-ruby/2023/04/msg00013.html
Slug: 1681149313
Date: 2023-04-10 17:55
Author: Ana Guerrero Lopez
Status: published

Survey to gather feedback on the native Debian package of GitLab [https://lists.debian.org/debian-ruby/2023/04/msg00013.html](https://lists.debian.org/debian-ruby/2023/04/msg00013.html)
