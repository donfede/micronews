Title: Registration for #miniDebConf in Lisbon open https://lists.debian.org/debian-devel-announce/2023/01/msg00002.html
Slug: 1674130729
Date: 2023-01-19 12:18
Author: Laura Arjona Reina
Status: published

Registration for #miniDebConf in Lisbon open [https://lists.debian.org/debian-devel-announce/2023/01/msg00002.html](https://lists.debian.org/debian-devel-announce/2023/01/msg00002.html)
