Title: If you couldn't attend previous sessions in #MDCO2Gaming some videos are published already at https://meetings-archive.debian.net/pub/debian-meetings/2020/MiniDebConfOnline2-Gaming/ #debiangaming
Slug: 1606049350
Date: 2020-11-22 12:49
Author: Anupa Ann Joseph
Status: published

If you couldn't attend previous sessions in #MDCO2Gaming some videos are published already at [https://meetings-archive.debian.net/pub/debian-meetings/2020/MiniDebConfOnline2-Gaming/](https://meetings-archive.debian.net/pub/debian-meetings/2020/MiniDebConfOnline2-Gaming/) #debiangaming
