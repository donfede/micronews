Title: MiniDebConf Online #2: Gaming Edition comes to an end with the Closing session at 21:00 UTC https://mdco2.mini.debconf.org/schedule/venue/3/ #MDCO2Gaming #debiangaming
Slug: 1606081113
Date: 2020-11-22 21:38
Author: Anupa Ann Joseph
Status: published

MiniDebConf Online #2: Gaming Edition comes to an end with the Closing session at 21:00 UTC [https://mdco2.mini.debconf.org/schedule/venue/3/](https://mdco2.mini.debconf.org/schedule/venue/3/) #MDCO2Gaming #debiangaming
