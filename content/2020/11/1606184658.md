Title: MiniDebConf Brasil Online 2020 takes place next weekend - November 28th and 29th https://lists.debian.org/debian-devel-announce/2020/11/msg00006.html
Slug: 1606184658
Date: 2020-11-24 02:24
Author: Paulo Henrique de Lima Santana (phls)
Status: published

MiniDebConf Brasil Online 2020 takes place next weekend - November 28th and 29th [https://lists.debian.org/debian-devel-announce/2020/11/msg00006.html](https://lists.debian.org/debian-devel-announce/2020/11/msg00006.html)
