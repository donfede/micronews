Title: A próxima atividade da MiniDebConf Online Brasil Um raio-x do Debian Brasília: Pequenas ações que transformaram uma comunidade nacional com Arthur Diniz, Francisco Penha, Sérgio de Almeida e Edson Soares. Assista online a partir das 18h40 no site https://mdcobr2020.debian.net/schedule/venue/1/  #Debian #MDCObr2020 #MDCOBR #MiniDebConf
Slug: 1606686748
Date: 2020-11-29 21:52
Author: Paulo Henrique de Lima Santana (phls)
Status: published

A próxima atividade da MiniDebConf Online Brasil Um raio-x do Debian Brasília: Pequenas ações que transformaram uma comunidade nacional com Arthur Diniz, Francisco Penha, Sérgio de Almeida e Edson Soares. Assista online a partir das 18h40 no site [https://mdcobr2020.debian.net/schedule/venue/1/](https://mdcobr2020.debian.net/schedule/venue/1/)  #Debian #MDCObr2020 #MDCOBR #MiniDebConf
