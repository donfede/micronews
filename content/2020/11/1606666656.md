Title: MiniDebConf Online Brasil 2020: estamos no segundo e último dia do evento e nossa transmissão começará às 14h com mais 6 atividades. Veja a programação completa em https://mdcobr2020.debian.net/programacao/grade/  #Debian #MDCObr2020 #MDCOBR #MiniDebConf
Slug: 1606666656
Date: 2020-11-29 16:17
Author: Paulo Henrique de Lima Santana (phls)
Status: published

MiniDebConf Online Brasil 2020: estamos no segundo e último dia do evento e nossa transmissão começará às 14h com mais 6 atividades. Veja a programação completa em [https://mdcobr2020.debian.net/programacao/grade/](https://mdcobr2020.debian.net/programacao/grade/)  #Debian #MDCObr2020 #MDCOBR #MiniDebConf
