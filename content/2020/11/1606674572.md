Title: A próxima atividade da MiniDebConf Online Brasil será Faxina no seu Debian com nosso DM Paulo Kretcheu. Assista online a partir das 15h10 no site https://mdcobr2020.debian.net/schedule/venue/1/   #Debian #MDCObr2020 #MDCOBR #MiniDebConf
Slug: 1606674572
Date: 2020-11-29 18:29
Author: Paulo Henrique de Lima Santana (phls)
Status: published

A próxima atividade da MiniDebConf Online Brasil será Faxina no seu Debian com nosso DM Paulo Kretcheu. Assista online a partir das 15h10 no site [https://mdcobr2020.debian.net/schedule/venue/1/](https://mdcobr2020.debian.net/schedule/venue/1/)   #Debian #MDCObr2020 #MDCOBR #MiniDebConf
