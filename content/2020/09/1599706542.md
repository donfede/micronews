Title: Debian Project News - September 9th, 2020: Debian Communication channels, New DPL, Internal and External news, DebConf20 and events, Reports, and Calls for Help. https://www.debian.org/News/weekly/2020/01/
Slug: 1599706542
Date: 2020-09-10 02:55
Author: Donald Norwood
Status: published

Debian Project News - September 9th, 2020: Debian Communication channels, New DPL, Internal and External news, DebConf20 and events, Reports, and Calls for Help. [https://www.debian.org/News/weekly/2020/01/](https://www.debian.org/News/weekly/2020/01/)
