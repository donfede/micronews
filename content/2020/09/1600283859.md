Title: Debian Local Groups at #DebConf20 and beyond https://bits.debian.org/2020/09/debian-local-groups-debconf20.html
Slug: 1600283859
Date: 2020-09-16 19:17
Author: Laura Arjona Reina
Status: published

Debian Local Groups at #DebConf20 and beyond [https://bits.debian.org/2020/09/debian-local-groups-debconf20.html](https://bits.debian.org/2020/09/debian-local-groups-debconf20.html)
