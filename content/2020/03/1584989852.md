Title: New Debian Developers and Maintainers (January and February 2020) https://bits.debian.org/2020/03/new-developers-2020-02.html
Slug: 1584989852
Date: 2020-03-23 18:57
Author: Jean-Pierre Giraud
Status: published

New Debian Developers and Maintainers (January and February 2020) [https://bits.debian.org/2020/03/new-developers-2020-02.html](https://bits.debian.org/2020/03/new-developers-2020-02.html)
