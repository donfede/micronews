Title:  #DebConf20 registration is open! https://bits.debian.org/2020/05/debconf20-open-registration-bursary.html
Slug: 1590406917
Date: 2020-05-25 11:41
Author: Laura Arjona Reina
Status: published

 #DebConf20 registration is open! [https://bits.debian.org/2020/05/debconf20-open-registration-bursary.html](https://bits.debian.org/2020/05/debconf20-open-registration-bursary.html)
