Title: Debian Installer Bullseye Alpha 3 release https://lists.debian.org/debian-devel-announce/2020/12/msg00001.html
Slug: 1607282268
Date: 2020-12-06 19:17
Author: Laura Arjona Reina
Status: published

Debian Installer Bullseye Alpha 3 release [https://lists.debian.org/debian-devel-announce/2020/12/msg00001.html](https://lists.debian.org/debian-devel-announce/2020/12/msg00001.html)
