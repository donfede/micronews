Title: At 11:00 UTC, "Debian Outreach Projects: Google Summer of Code 2020". This talk will showcase a few of the projects that students are currently working on with Debian under #GSoC 2020 with #Debian https://debconf20.debconf.org/talks/71-debian-outreach-projects-google-summer-of-code-2020/
Slug: 1598618781
Date: 2020-08-28 12:46
Author: Laura Arjona Reina
Status: published

At 11:00 UTC, "Debian Outreach Projects: Google Summer of Code 2020". This talk will showcase a few of the projects that students are currently working on with Debian under #GSoC 2020 with #Debian [https://debconf20.debconf.org/talks/71-debian-outreach-projects-google-summer-of-code-2020/](https://debconf20.debconf.org/talks/71-debian-outreach-projects-google-summer-of-code-2020/)
