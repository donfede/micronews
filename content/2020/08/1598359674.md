Title: At 11:00 UTC please join us for "What's new in the Linux kernel (and what's missing in Debian)" https://debconf20.debconf.org/talks/19-whats-new-in-the-linux-kernel-and-whats-missing-in-debian/ #DebConf20
Slug: 1598359674
Date: 2020-08-25 12:47
Author: Laura Arjona Reina
Status: published

At 11:00 UTC please join us for "What's new in the Linux kernel (and what's missing in Debian)" [https://debconf20.debconf.org/talks/19-whats-new-in-the-linux-kernel-and-whats-missing-in-debian/](https://debconf20.debconf.org/talks/19-whats-new-in-the-linux-kernel-and-whats-missing-in-debian/) #DebConf20
