Title: Lenovo, Infomaniak, Google and Amazon Web Services (AWS), Platinum Sponsors of #DebConf20 https://bits.debian.org/2020/08/lenovo-infomaniak-google-aws-platinum-debconf20.html
Slug: 1597955187
Date: 2020-08-20 20:26
Author: Laura Arjona Reina
Status: published

Lenovo, Infomaniak, Google and Amazon Web Services (AWS), Platinum Sponsors of #DebConf20 [https://bits.debian.org/2020/08/lenovo-infomaniak-google-aws-platinum-debconf20.html](https://bits.debian.org/2020/08/lenovo-infomaniak-google-aws-platinum-debconf20.html)
