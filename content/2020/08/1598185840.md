Title: Next talk at #DebConf20 online starts at 14:00 UTC: "Doing things /together/", followed by "Forwards to a Federated Future" and then the Debian Diversity BoF https://debconf20.debconf.org/schedule/?block=1
Slug: 1598185840
Date: 2020-08-23 12:30
Author: urbec
Status: published

Next talk at #DebConf20 online starts at 14:00 UTC: "Doing things /together/", followed by "Forwards to a Federated Future" and then the Debian Diversity BoF [https://debconf20.debconf.org/schedule/?block=1](https://debconf20.debconf.org/schedule/?block=1)
