Title: Today from 11:00 to 20:45 UTC there will be a Puppet Team Sprint. Read the wiki page about how to participate https://wiki.debian.org/DebConf/20/Sprints
Slug: 1598618983
Date: 2020-08-28 12:49
Author: Laura Arjona Reina
Status: published

Today from 11:00 to 20:45 UTC there will be a Puppet Team Sprint. Read the wiki page about how to participate [https://wiki.debian.org/DebConf/20/Sprints](https://wiki.debian.org/DebConf/20/Sprints)
