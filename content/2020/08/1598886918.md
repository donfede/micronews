Title: Debian Academy: a new initiative to define and run an official Debian e-learning platform with Debian specific courses. Do you want to help make it happen? Visit the wiki page and join the team https://wiki.debian.org/DebianAcademy
Slug: 1598886918
Date: 2020-08-31 15:15
Author: Laura Arjona Reina
Status: published

Debian Academy: a new initiative to define and run an official Debian e-learning platform with Debian specific courses. Do you want to help make it happen? Visit the wiki page and join the team [https://wiki.debian.org/DebianAcademy](https://wiki.debian.org/DebianAcademy)
