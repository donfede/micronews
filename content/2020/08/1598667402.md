Title: All of the sessions for #DebConf20 day 6 have ended. Thank you to all of our contributors and viewers, and to our DebConf Video team! https://debconf20.debconf.org/schedule/?block=7
Slug: 1598667402
Date: 2020-08-29 02:16
Author: Donald Norwood
Status: published

All of the sessions for #DebConf20 day 6 have ended. Thank you to all of our contributors and viewers, and to our DebConf Video team! [https://debconf20.debconf.org/schedule/?block=7](https://debconf20.debconf.org/schedule/?block=7)
