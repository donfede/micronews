Title: In the traditional "Bits from the DPL" Jonathan Cristopher Carter takes a look at the state of the Debian project and where we’re heading, starting at 16:00 UTC at #DebConf20 online. Follow the streaming at https://debconf20.debconf.org/schedule/venue/1/
Slug: 1598290318
Date: 2020-08-24 17:31
Author: urbec
Status: published

In the traditional "Bits from the DPL" Jonathan Cristopher Carter takes a look at the state of the Debian project and where we’re heading, starting at 16:00 UTC at #DebConf20 online. Follow the streaming at [https://debconf20.debconf.org/schedule/venue/1/](https://debconf20.debconf.org/schedule/venue/1/)
