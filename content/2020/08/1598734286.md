Title: What can we do to help support existing or upcoming Debian local groups, or help spark interest in more parts of the world? Come to the #DebConf20 discussion session about "Local Teams" at 19:00 UTC https://debconf20.debconf.org/talks/50-local-teams/
Slug: 1598734286
Date: 2020-08-29 20:51
Author: Donald Norwood
Status: published

What can we do to help support existing or upcoming Debian local groups, or help spark interest in more parts of the world? Come to the #DebConf20 discussion session about "Local Teams" at 19:00 UTC [https://debconf20.debconf.org/talks/50-local-teams/](https://debconf20.debconf.org/talks/50-local-teams/)
