Title: Thursday is the fifth day of #DebConf20 and starts at 10:00 with the short talk "Flexible Record keeping in a FOSS world" followed at 10:30 UTC by another short talk "The Debian Janitor" https://debconf20.debconf.org/schedule/?block=5
Slug: 1598528014
Date: 2020-08-27 11:33
Author: Laura Arjona Reina
Status: published

Thursday is the fifth day of #DebConf20 and starts at 10:00 with the short talk "Flexible Record keeping in a FOSS world" followed at 10:30 UTC by another short talk "The Debian Janitor" [https://debconf20.debconf.org/schedule/?block=5](https://debconf20.debconf.org/schedule/?block=5)
