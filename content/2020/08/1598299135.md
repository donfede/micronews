Title: Participa en la #DebConf20 en español a partir de las 18:00h UTC. Hoy,  "Shell ZSH en Debian" (18:00 UTC) seguido de "¿Puedo usar limpiamente Debian en las Raspberry Pi?" (19:00 UTC) y "Construcciones reproducibles en Debian (Debian Reproducible Builds): Un camino verificable desde el origen hasta el binario" (20:00 UTC) https://debconf20.debconf.org/schedule/?block=2
Slug: 1598299135
Date: 2020-08-24 19:58
Author: Laura Arjona Reina
Status: published

Participa en la #DebConf20 en español a partir de las 18:00h UTC. Hoy,  "Shell ZSH en Debian" (18:00 UTC) seguido de "¿Puedo usar limpiamente Debian en las Raspberry Pi?" (19:00 UTC) y "Construcciones reproducibles en Debian (Debian Reproducible Builds): Un camino verificable desde el origen hasta el binario" (20:00 UTC) [https://debconf20.debconf.org/schedule/?block=2](https://debconf20.debconf.org/schedule/?block=2)
