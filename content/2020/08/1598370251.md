Title:  #DebConf20 continues at 14:00 UTC with "Introduction to GNU/Lunix in Education"  https://debconf20.debconf.org/talks/18-introduction-to-gnulinux-in-education/
Slug: 1598370251
Date: 2020-08-25 15:44
Author: Laura Arjona Reina
Status: published

 #DebConf20 continues at 14:00 UTC with "Introduction to GNU/Lunix in Education"  [https://debconf20.debconf.org/talks/18-introduction-to-gnulinux-in-education/](https://debconf20.debconf.org/talks/18-introduction-to-gnulinux-in-education/)
