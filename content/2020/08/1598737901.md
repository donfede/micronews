Title: At 20:00 UTC we'll have the talk "Introducing the Principles of Digital Autonomy". Follow the streaming in #DebConf20 website https://debconf20.debconf.org/schedule/venue/1/
Slug: 1598737901
Date: 2020-08-29 21:51
Author: Donald Norwood
Status: published

At 20:00 UTC we'll have the talk "Introducing the Principles of Digital Autonomy". Follow the streaming in #DebConf20 website [https://debconf20.debconf.org/schedule/venue/1/](https://debconf20.debconf.org/schedule/venue/1/)
