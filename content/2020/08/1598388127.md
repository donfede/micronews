Title: At 19:00 UTC, Impress Improv session at #DebConf20 - This is a series of lightning talks based on someone else’s previously-unseen slides, on topics not necessarily related to Debian... Are you curious? Follow the streaming https://debconf20.debconf.org/schedule/venue/1/
Slug: 1598388127
Date: 2020-08-25 20:42
Author: Laura Arjona Reina
Status: published

At 19:00 UTC, Impress Improv session at #DebConf20 - This is a series of lightning talks based on someone else’s previously-unseen slides, on topics not necessarily related to Debian... Are you curious? Follow the streaming [https://debconf20.debconf.org/schedule/venue/1/](https://debconf20.debconf.org/schedule/venue/1/)
