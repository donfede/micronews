Title: Updated Debian 10: 10.5 released https://www.debian.org/News/2020/20200801
Slug: 1596303504
Date: 2020-08-01 17:38
Author: Donald Norwood
Status: published

Updated Debian 10: 10.5 released [https://www.debian.org/News/2020/20200801](https://www.debian.org/News/2020/20200801)
