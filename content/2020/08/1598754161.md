Title: Did you attend #DebConf20 and want to share your experience about it? We appreciate feedback about what worked well and the things we could improve, please send your comments to feedback at _debconf_dot_ org.
Slug: 1598754161
Date: 2020-08-30 02:22
Author: Donald Norwood
Status: published

Did you attend #DebConf20 and want to share your experience about it? We appreciate feedback about what worked well and the things we could improve, please send your comments to feedback at _debconf_dot_ org.
