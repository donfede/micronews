Title:  #DebConf20 continues with "FreedomBox: The Home Server Appliance" and the Community Team BoF at the same time (19:00 UTC). Links to both streamings in the website https://debconf20.debconf.org/
Slug: 1598474850
Date: 2020-08-26 20:47
Author: Laura Arjona Reina
Status: published

 #DebConf20 continues with "FreedomBox: The Home Server Appliance" and the Community Team BoF at the same time (19:00 UTC). Links to both streamings in the website [https://debconf20.debconf.org/](https://debconf20.debconf.org/)
