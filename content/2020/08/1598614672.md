Title: The first talk in #DebConf20 today is "The Practical Programming Language, or Why C++ Integers are Fundamentally Broken" at 10:00 UTC https://debconf20.debconf.org/talks/68-the-practical-programming-language-or-why-c-integers-are-fundamentally-broken/
Slug: 1598614672
Date: 2020-08-28 11:37
Author: Laura Arjona Reina
Status: published

The first talk in #DebConf20 today is "The Practical Programming Language, or Why C++ Integers are Fundamentally Broken" at 10:00 UTC [https://debconf20.debconf.org/talks/68-the-practical-programming-language-or-why-c-integers-are-fundamentally-broken/](https://debconf20.debconf.org/talks/68-the-practical-programming-language-or-why-c-integers-are-fundamentally-broken/)
