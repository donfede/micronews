Title: Debian 8 Long Term Support reaching end-of-life https://www.debian.org/News/2020/20200709 #jessie #LTS
Slug: 1594319203
Date: 2020-07-09 18:26
Author: Laura Arjona Reina
Status: published

Debian 8 Long Term Support reaching end-of-life [https://www.debian.org/News/2020/20200709](https://www.debian.org/News/2020/20200709) #jessie #LTS
