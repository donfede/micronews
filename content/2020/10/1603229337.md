Title: MiniDebConfOnline, Games Edition is accepting submissions for talks until the 30th of October. https://lists.debian.org/debconf-announce/2020/10/msg00001.html
Slug: 1603229337
Date: 2020-10-20 21:28
Author: Donald Norwood
Status: published

MiniDebConfOnline, Games Edition is accepting submissions for talks until the 30th of October. [https://lists.debian.org/debconf-announce/2020/10/msg00001.html](https://lists.debian.org/debconf-announce/2020/10/msg00001.html)
