Title: Contribute!
Slug: contribute
This is a [Debian][debian] micronews service complementing other services such as:

 * [Debian Press Release](https://www.debian.org/News/)
 * [Debian Project News](https://www.debian.org/News/weekly/)
 * [Debian Bits, the Debian Blog](https://bits.debian.org)


Micronews is powered by [Pelican][pelican]. You can find the templates and theme used on this blog in [our (Git) repository][gitdo]. Patches, proposals for new themes and constructive suggestions are welcome!

## How to contribute a news item

Everybody is welcome to submit any item that they consider will be useful.  The publicity team is in charge of moderating and publishing. Remember the news items should be short, and should almost always include a URL.

### If you're a Debian Developer

Clone the git repository (you already have write access):

    $ git clone git@salsa.debian.org:publicity-team/micronews.git

Use the `add.py` script.  (`add.py` requires that Python 3 be installed in your system):

    $ ./add.py -a "Ana Guerrero López" -t "Debian and Tor Services available as Onion Services https://bits.debian.org/2016/08/debian-and-tor-services-available-as-onion-services.html"

Then commit and push, or simply press "y" to push immediately if you are sure.

The publicity team will see your commit and will push the micronews if there is not any issue with it. You can send a ping in `#debian-publicity` if it's urgent or it has not been published after one or two days.

### If you're not a Debian Developer

You have several options:

* Request write access to the [Publicity team in Salsa](https://salsa.debian.org/publicity-team). Once it's granted to you, follow the steps listed above for Debian Developers.

* Contact us using IRC in channel `#debian-publicity` on the OFTC network, and ask us there.

* Send a mail to [debian-publicity@lists.debian.org](https://lists.debian.org/debian-publicity/) and ask your item to be included in micronews. This is a publicly archived list.


## How to contribute other changes

If the change is small and you have write access to git, feel free to commit it directly.

Otherwise, please open a bug report in the [Debian Bug Tracking System](https://bugs.debian.org) against the pseudo-package [press](https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=press)

## License

This site is under the same license and copyright as the [Debian][debian]
website, see [Debian WWW pages license][wwwlicense].

## Contact us

If you want to contact us, please send an email to the [debian-publicity
mailing list][debian-publicity]. This is a publicly archived list.



[pelican]: http://getpelican.com/ "Find out about Pelican"
[debian]: https://www.debian.org "Debian - The Universal Operating System"
[gitdo]: https://salsa.debian.org/publicity-team/micronews
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
