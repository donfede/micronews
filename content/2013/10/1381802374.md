Title: None
Slug: 1381802374
Date: 2013-10-15 01:59:34
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/Ljpg_kkZSbq_XWM-3eU9Sg

Happy Ada Lovelace Day! Meet some of the women behind Debian at [https://bits.debian.org/2013/10/ada-lovelace-day.html](https://bits.debian.org/2013/10/ada-lovelace-day.html)

