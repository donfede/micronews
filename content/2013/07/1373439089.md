Title: None
Slug: 1373439089
Date: 2013-07-10 06:51:29
Author: imported from identi.ca/debian
Status: published
identica: http://identi.ca/notice/100745655

First alpha release of Debian Edu/Skolelinux based on #wheezy [https://lists.debian.org/debian-edu/2013/04/msg00132.html](https://lists.debian.org/debian-edu/2013/04/msg00132.html)
