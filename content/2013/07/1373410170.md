Title: None
Slug: 1373410170
Date: 2013-07-09 22:49:30
Author: imported from identi.ca/debian
Status: published
identica: http://identi.ca/notice/99910632

This year's 5th issue of the Debian Project News is out: read it at [https://www.debian.org/News/weekly/2013/05/](https://www.debian.org/News/weekly/2013/05/)  #DPN
