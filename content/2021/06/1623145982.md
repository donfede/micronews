Title: Registration for #DebConf21 Online is Open https://bits.debian.org/2021/06/debconf21-open-registration.html
Slug: 1623145982
Date: 2021-06-08 09:53
Author: Laura Arjona Reina
Status: published

Registration for #DebConf21 Online is Open [https://bits.debian.org/2021/06/debconf21-open-registration.html](https://bits.debian.org/2021/06/debconf21-open-registration.html)
