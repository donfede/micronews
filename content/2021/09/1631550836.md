Title: New Debian Developers and Maintainers (July and August 2021) https://bits.debian.org/2021/09/new-developers-2021-08.html
Slug: 1631550836
Date: 2021-09-13 16:33
Author: Jean-Pierre Giraud
Status: published

New Debian Developers and Maintainers (July and August 2021) [https://bits.debian.org/2021/09/new-developers-2021-08.html](https://bits.debian.org/2021/09/new-developers-2021-08.html)
