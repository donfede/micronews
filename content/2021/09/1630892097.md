Title: "Go" for MiniDebConf 2021 Regensburg (Germany), October 2nd-3rd! Send your talk proposals until September 24th - And don't forget to register! https://lists.debian.org/debian-devel-announce/2021/09/msg00001.html
Slug: 1630892097
Date: 2021-09-06 01:34
Author: Jean-Pierre Giraud
Status: published

"Go" for MiniDebConf 2021 Regensburg (Germany), October 2nd-3rd! Send your talk proposals until September 24th - And don't forget to register! [https://lists.debian.org/debian-devel-announce/2021/09/msg00001.html](https://lists.debian.org/debian-devel-announce/2021/09/msg00001.html)
