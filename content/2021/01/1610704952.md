Title: Virtual Debian booth at FOSDEM 2021, 6 and 7 February, https://wiki.debian.org/DebianEvents/be/2021/FOSDEM
Slug: 1610704952
Date: 2021-01-15 10:02
Author: Joost van Baal-Ilić
Status: published

Virtual Debian booth at FOSDEM 2021, 6 and 7 February, [https://wiki.debian.org/DebianEvents/be/2021/FOSDEM](https://wiki.debian.org/DebianEvents/be/2021/FOSDEM)
