Title: Events in the "Rex" online venue for the forenoon session start at 11:30 IST and are in Telugu: "డెబియన్ తెలుగు - స్వేచ్ఛ ప్రాజెక్టు కథాకమామిషు", "డెబియన్ ప్రాజెక్ట్ - అంతర్జాతీయ ఆపరేటింగ్ సిస్టంగా దాని రూపాంతరం", "Salsa CI - డెబియన్ ప్యాకేజీలు ఆటోమేట్ చేయడం ఎలాగో అని తెలుగులో తెలుసుకుందాము", and BoF: "FOSS Telugu Localisation" - Rex: https://in2021.mini.debconf.org/schedule/venue/6/ #MiniDebConf #debian #freesoftware #DebianIndia #MDCO-IN-2021
Slug: 1611382197
Date: 2021-01-23 06:09
Author: Anupa Ann Joseph
Status: published

Events in the "Rex" online venue for the forenoon session start at 11:30 IST and are in Telugu: "డెబియన్ తెలుగు - స్వేచ్ఛ ప్రాజెక్టు కథాకమామిషు", "డెబియన్ ప్రాజెక్ట్ - అంతర్జాతీయ ఆపరేటింగ్ సిస్టంగా దాని రూపాంతరం", "Salsa CI - డెబియన్ ప్యాకేజీలు ఆటోమేట్ చేయడం ఎలాగో అని తెలుగులో తెలుసుకుందాము", and BoF: "FOSS Telugu Localisation" - Rex: [https://in2021.mini.debconf.org/schedule/venue/6/](https://in2021.mini.debconf.org/schedule/venue/6/) #MiniDebConf #debian #freesoftware #DebianIndia #MDCO-IN-2021
