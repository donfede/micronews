Title: APT 2.2.0 released https://blog.jak-linux.org/2021/02/18/apt-2.2/
Slug: 1613747842
Date: 2021-02-19 15:17
Author: Donald Norwood
Status: published

APT 2.2.0 released [https://blog.jak-linux.org/2021/02/18/apt-2.2/](https://blog.jak-linux.org/2021/02/18/apt-2.2/)
