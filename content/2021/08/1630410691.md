Title: Did you attend #DebConf21 and want to share your experience about it? We appreciate feedback about what worked well and the things we could improve, please send your comments to feedback_at_debconf_dot_org 
Slug: 1630410691
Date: 2021-08-31 11:51
Author: Laura Arjona Reina
Status: published

Did you attend #DebConf21 and want to share your experience about it? We appreciate feedback about what worked well and the things we could improve, please send your comments to feedback_at_debconf_dot_org 
