Title: Call for proposals for the upcoming MiniDebConf 2021 Regensburg https://lists.debian.org/debian-project/2021/08/msg00035.html - And don't forget to register!
Slug: 1629727322
Date: 2021-08-23 14:02
Author: Jean-Pierre Giraud
Status: published

Call for proposals for the upcoming MiniDebConf 2021 Regensburg [https://lists.debian.org/debian-project/2021/08/msg00035.html](https://lists.debian.org/debian-project/2021/08/msg00035.html) - And don't forget to register!
