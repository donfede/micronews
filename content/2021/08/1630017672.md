Title: At 21:00 UTC, 'Making use of snapshot.debian.org for fun and profit' in #DebConf21 channel one, and the 'Debian Med BoF' in channel two. Follow the live streaming at https://debconf21.debconf.org/
Slug: 1630017672
Date: 2021-08-26 22:41
Author: Laura Arjona Reina
Status: published

At 21:00 UTC, 'Making use of snapshot.debian.org for fun and profit' in #DebConf21 channel one, and the 'Debian Med BoF' in channel two. Follow the live streaming at [https://debconf21.debconf.org/](https://debconf21.debconf.org/)
