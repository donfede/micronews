Title: 'Calling All FLOSS Founders', about the start of free software projects and their development, is the next talk at #DebConf21, at 20:00 UTC, https://debconf21.debconf.org/talks/25-calling-all-floss-founders/
Slug: 1629835255
Date: 2021-08-24 20:30
Author: Jean-Pierre Giraud
Status: published

'Calling All FLOSS Founders', about the start of free software projects and their development, is the next talk at #DebConf21, at 20:00 UTC, [https://debconf21.debconf.org/talks/25-calling-all-floss-founders/](https://debconf21.debconf.org/talks/25-calling-all-floss-founders/)
