Title: CD testing is a great way to contribute to the release and get stuck into Debian. liz in Brisbane, AU has just filed her first ever bug report testing a live image! https://wiki.debian.org/Teams/DebianCd  #ReleasingDebianBullseye
Slug: 1628963962
Date: 2021-08-14 17:59
Author: Jonathan Wiltshire
Status: published

CD testing is a great way to contribute to the release and get stuck into Debian. liz in Brisbane, AU has just filed her first ever bug report testing a live image! [https://wiki.debian.org/Teams/DebianCd](https://wiki.debian.org/Teams/DebianCd)  #ReleasingDebianBullseye
