Title: Debian's CD team is warming up their build server - did you know it uses 88 CPUs, 377GiB of RAM and over 8TiB of SSDs to generate images for bullseye's 9 architectures? #ReleasingDebianBullseye
Slug: 1628939409
Date: 2021-08-14 11:10
Author: Jonathan Wiltshire
Status: published

Debian's CD team is warming up their build server - did you know it uses 88 CPUs, 377GiB of RAM and over 8TiB of SSDs to generate images for bullseye's 9 architectures? #ReleasingDebianBullseye
