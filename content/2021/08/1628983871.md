Title: For bullseye, the security suite is now named bullseye-security so users should adapt their APT source-list files and configuration accordingly when upgrading. More details on https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#security-archive
Slug: 1628983871
Date: 2021-08-14 23:31
Author: Laura Arjona Reina
Status: published

For bullseye, the security suite is now named bullseye-security so users should adapt their APT source-list files and configuration accordingly when upgrading. More details on [https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#security-archive](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#security-archive)
