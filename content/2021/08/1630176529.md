Title: autopkgtest, a test automation tool for Debian packages, is a central part of the contemporary Debian release process and quality control efforts. The activity "autopkgtest office hours" (17:00 UTC) aim to help fellow maintainers with their questions about implementing or improving autopkgtest support for their packages, https://debconf21.debconf.org/schedule/venue/1/
Slug: 1630176529
Date: 2021-08-28 18:48
Author: Laura Arjona Reina
Status: published

autopkgtest, a test automation tool for Debian packages, is a central part of the contemporary Debian release process and quality control efforts. The activity "autopkgtest office hours" (17:00 UTC) aim to help fellow maintainers with their questions about implementing or improving autopkgtest support for their packages, [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)
