Title: The two next talks of #DebConf21 start at 13:00 UTC, 'branch2repo -- enabling casual contributions to debian-installer', and 'బాల స్వేచ్ఛ ప్రాజెక్టు - కంప్యూటర్ ఆధారిత విద్యలో ఒక కొత్త ఒరవడి',  about Internationalization, Localization and Accessibility, on the second channel, https://debconf21.debconf.org/schedule/?block=1
Slug: 1629815145
Date: 2021-08-24 14:25
Author: Jean-Pierre Giraud
Status: published

The two next talks of #DebConf21 start at 13:00 UTC, 'branch2repo -- enabling casual contributions to debian-installer', and 'బాల స్వేచ్ఛ ప్రాజెక్టు - కంప్యూటర్ ఆధారిత విద్యలో ఒక కొత్త ఒరవడి',  about Internationalization, Localization and Accessibility, on the second channel, [https://debconf21.debconf.org/schedule/?block=1](https://debconf21.debconf.org/schedule/?block=1)
