Title: Thank you to all the people who made #DebConf21 possible! The DebConf organisation team, Debian Video Team, our speakers, volunteers, and attendees. Would you like to join for organising the next DebConf? If so visit https://wiki.debian.org/DebConf/22 and contact the DebConf team.
Slug: 1630188720
Date: 2021-08-28 22:12
Author: Laura Arjona Reina
Status: published

Thank you to all the people who made #DebConf21 possible! The DebConf organisation team, Debian Video Team, our speakers, volunteers, and attendees. Would you like to join for organising the next DebConf? If so visit [https://wiki.debian.org/DebConf/22](https://wiki.debian.org/DebConf/22) and contact the DebConf team.
