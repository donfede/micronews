Title: Releasing Debian 11 doesn't mean Debian 10 "buster" is abandoned. Debian supports the previous version for at least twelve months following a new release, before it moves to the LTS and eLTS teams for further maintenance #ReleasingDebianBullseye
Slug: 1628962799
Date: 2021-08-14 17:39
Author: Laura Arjona Reina
Status: published

Releasing Debian 11 doesn't mean Debian 10 "buster" is abandoned. Debian supports the previous version for at least twelve months following a new release, before it moves to the LTS and eLTS teams for further maintenance #ReleasingDebianBullseye
