Title: The Debian Project mourns the loss of Robert Lemmen, Karl Ramm and Rogério Theodoro de Brito https://www.debian.org/News/2021/20210812
Slug: 1628816103
Date: 2021-08-13 00:55
Author: Laura Arjona Reina
Status: published

The Debian Project mourns the loss of Robert Lemmen, Karl Ramm and Rogério Theodoro de Brito [https://www.debian.org/News/2021/20210812](https://www.debian.org/News/2021/20210812)
