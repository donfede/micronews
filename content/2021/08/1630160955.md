Title: At 13:00 UTC on channel one, two short talks: "Latest updates about fabre.debian.net - Finding untouched bugs" targetted to contributor or maintainer who want to fix bugs which are not related to packages they maintain, followed at 13:30 UTC by "proposing a new d-i disk preparation tool, 'growlight' ", https://debconf21.debconf.org/schedule/venue/1/
Slug: 1630160955
Date: 2021-08-28 14:29
Author: Laura Arjona Reina
Status: published

At 13:00 UTC on channel one, two short talks: "Latest updates about fabre.debian.net - Finding untouched bugs" targetted to contributor or maintainer who want to fix bugs which are not related to packages they maintain, followed at 13:30 UTC by "proposing a new d-i disk preparation tool, 'growlight' ", [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)
