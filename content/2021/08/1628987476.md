Title: win32-loader, which enables Debian installation from Windows without use of separate installation media, now supports UEFI and Secure Boot  #NewInDebianBullseye #ReleasingDebianBullseye
Slug: 1628987476
Date: 2021-08-15 00:31
Author: Donald Norwood
Status: published


win32-loader, which enables Debian installation from Windows without use of separate installation media, now supports UEFI and Secure Boot  #NewInDebianBullseye #ReleasingDebianBullseye
