Title:  #DebConf21 continues at 17:00 UTC with 'use Perl; # Annual meeting of the Debian Perl Group', an opportunity for pkg-perl team to meet in person for discussing current topics and planning future work, and the talk 'One year of pandemie - lessons learned in Debian Med team' at the same time on channel two, https://debconf21.debconf.org/schedule/?block=2
Slug: 1629913581
Date: 2021-08-25 17:46
Author: Jean-Pierre Giraud
Status: published

 #DebConf21 continues at 17:00 UTC with 'use Perl; # Annual meeting of the Debian Perl Group', an opportunity for pkg-perl team to meet in person for discussing current topics and planning future work, and the talk 'One year of pandemie - lessons learned in Debian Med team' at the same time on channel two, [https://debconf21.debconf.org/schedule/?block=2](https://debconf21.debconf.org/schedule/?block=2)
