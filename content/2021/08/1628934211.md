Title: The bullseye version of the Debian Installer is being moved into its new home. The installer has a long history, you can read some of it at https://salsa.debian.org/installer-team/debian-installer/-/blob/master/doc/history.txt #ReleasingDebianBullseye
Slug: 1628934211
Date: 2021-08-14 09:43
Author: Jonathan Wiltshire
Status: published

The bullseye version of the Debian Installer is being moved into its new home. The installer has a long history, you can read some of it at [https://salsa.debian.org/installer-team/debian-installer/-/blob/master/doc/history.txt](https://salsa.debian.org/installer-team/debian-installer/-/blob/master/doc/history.txt) #ReleasingDebianBullseye
