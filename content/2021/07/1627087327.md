Title: Debian bullseye release planned for 2021-08-14, what will happen in the last weeks up to the release. https://lists.debian.org/debian-devel-announce/2021/07/msg00003.html
Slug: 1627087327
Date: 2021-07-24 00:42
Author: Donald Norwood
Status: published

Debian bullseye release planned for 2021-08-14, what will happen in the last weeks up to the release. [https://lists.debian.org/debian-devel-announce/2021/07/msg00003.html](https://lists.debian.org/debian-devel-announce/2021/07/msg00003.html)
