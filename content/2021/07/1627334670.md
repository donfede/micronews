Title: @Gitlab is sponsoring #DebConf21 (online, August 22-29) at the Silver level, and Debian is an Open Source Partner at #GitLabCommit (virtual, August 3-4, 2021, registration is free of cost) https://gitlabcommitvirtual2021.com/
Slug: 1627334670
Date: 2021-07-26 21:24
Author: Donald Norwood
Status: published

@Gitlab is sponsoring #DebConf21 (online, August 22-29) at the Silver level, and Debian is an Open Source Partner at #GitLabCommit (virtual, August 3-4, 2021, registration is free of cost) [https://gitlabcommitvirtual2021.com/](https://gitlabcommitvirtual2021.com/)
