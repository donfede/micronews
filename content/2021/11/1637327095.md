Title: New Debian Developers and Maintainers (September and October 2021) https://bits.debian.org/2021/11/new-developers-2021-10.html
Slug: 1637327095
Date: 2021-11-19 13:04
Author: Laura Arjona Reina
Status: published

New Debian Developers and Maintainers (September and October 2021) [https://bits.debian.org/2021/11/new-developers-2021-10.html](https://bits.debian.org/2021/11/new-developers-2021-10.html)
