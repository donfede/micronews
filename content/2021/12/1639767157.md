Title: 2,000 fonts for Debian https://bits.debian.org/2021/12/2000-fonts-debian.html
Slug: 1639767157
Date: 2021-12-17 18:52
Author: Laura Arjona Reina
Status: published

2,000 fonts for Debian [https://bits.debian.org/2021/12/2000-fonts-debian.html](https://bits.debian.org/2021/12/2000-fonts-debian.html)
