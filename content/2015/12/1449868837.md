Title: Bits from the Debian Continuous Integration project
Slug: 1449868837
Date: 2015-12-11 21:20:37
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/DznLpqGETkG8I2vM11Yy8w

Bits from the Debian Continuous Integration project: Infrastructure upgrade, lxc as testing backend, and more: <br /><a href="https://lists.debian.org/debian-devel-announce/2015/12/msg00002.html">https://lists.debian.org/debian-devel-announce/2015/12/msg00002.html</a> 

