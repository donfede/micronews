Title: Thanks wiki editors!
Slug: 1451145018
Date: 2015-12-26 15:50:18
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/U0lhkxhJQKO9LvYsZnNEFg

Do you help improving <a href="https://wiki.debian.org">wiki.debian.org</a>? Thanks! <a href="https://alioth.debian.org/account/register.php">Create an Alioth account</a>, and you'll be credited in <a href="https://contributors.debian.org">contributors.debian.org</a> for your wiki editions!

