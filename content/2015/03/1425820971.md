Title: Status of the Debian jessie release
Slug: 1425820971
Date: 2015-03-08 13:22:51
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/4x17BTTIT-yWExS9PnOoUw

Want to help the jessie release? We <a href="https://lists.debian.org/20150308100556.A0C663B9E@thykier.net">need help</a> with the release notes and 72 RC bugs (especially the grub2 ones).

