Title: Debian Women MiniDebConf Bucharest next 16-17 May
Slug: 1430985225
Date: 2015-05-07 07:53:45
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/kPhse_gJR2m3XXfQQHKxJQ

<a href="http://bucharest2015.mini.debconf.org">Debian Women MiniDebConf Bucharest</a> next 16-17 May: <a href="https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2015/Participants">registration</a>  is still open, and your support counts double thanks to a <a href="http://bucharest2015.mini.debconf.org/fundraising.shtml">fundraising match</a>!

