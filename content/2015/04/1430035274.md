Title: Releasing Debian jessie: surprise!
Slug: 1430035274
Date: 2015-04-26 08:01:14
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/O-sBbMpKSquo8eiaL_G09g

If any of the lines in your /etc/apt/sources.list refer to 'stable', you might get a surprise on your next upgrade! #releasingjessie

