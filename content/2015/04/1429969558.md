Title: Releasing Debian jessie: mirrors
Slug: 1429969558
Date: 2015-04-25 13:45:58
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/hJrrHw95SVeZlq_kzgt4Rg

Debian thanks our sponsors of about 50 <a href="https://www.debian.org/mirror/official_sponsors">primary</a> and over 400 <a href="https://www.debian.org/mirror/sponsors">total</a> <a href="https://www.debian.org/mirror/list">mirrors</a> for helping distribute jessie. #releasingjessie

