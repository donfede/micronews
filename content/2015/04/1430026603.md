Title: Releasing Debian jessie: thanks to our partners
Slug: 1430026603
Date: 2015-04-26 05:36:43
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/-3JS9f3qQBexm0cmPnTPRA

Debian would like to thank our 21 <a href="https://www.debian.org/partners/">partner organisations</a>, who provide hardware, employ developers, donate money, hosting and more. #releasingjessie

