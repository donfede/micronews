Title: Releasing Debian jessie: Java
Slug: 1430045393
Date: 2015-04-26 10:49:53
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/XFX6cLl9QHqNdsqZTcQh8A

New <a href="https://www.debian.org/releases/jessie/amd64/release-notes/ch-whats-new.en.html#debian-java">Java tools in jessie</a> include Tomcat 7/8, VisualVM, the Dynamic Code Evolution VM, Gradle, eclipse-wtp-webtools, closure-compiler and more. #releasingjessie

