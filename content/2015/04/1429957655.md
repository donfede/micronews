Title: Releasing Debian jessie: win32-loader
Slug: 1429957655
Date: 2015-04-25 10:27:35
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/Il_48_AqRZ6_mfX8tXWD5w

The win32-loader directories have been renamed for the change of jessie to stable, wheezy to oldstable and squeeze to oldoldstable. #releasingjessie

