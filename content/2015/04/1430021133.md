Title: Releasing Debian jessie: SSLv3 disabled.
Slug: 1430021133
Date: 2015-04-26 04:05:33
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/llQbpwh8RFOmh4cDGGzRIg

Legacy insecure SSLv3 has been disabled in this release, applications have been compiled or configured without support for this protocol.

