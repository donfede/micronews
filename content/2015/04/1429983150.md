Title: Releasing Debian jessie: debtags
Slug: 1429983150
Date: 2015-04-25 17:32:30
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/MXu_G4AkS-aWylBi2WRqBQ

There are 619 tags in the <a href="http://debtags.debian.net/reports/stats/">Debtags vocabulary</a>, and 62139 known packages to tag. Of these, 25065 are tagged by humans, and 37074 are tagged by robots. #releasingjessie

