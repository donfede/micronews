Title: Releasing Debian jessie: new non-uploading developers
Slug: 1429978539
Date: 2015-04-25 16:15:39
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/PLeuRDp8SkiX33HzJ6-BaQ

During the jessie development, 6 contributors became <a href="https://nm.debian.org/public/people">non-uploading Debian Developers</a>, supporting the community in non-packaging tasks. #releasingjessie

