Title: Releasing Debian jessie: party pics
Slug: 1430051993
Date: 2015-04-26 12:39:53
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/Y9HTXBBbRRqpJ9YxR_Otfg

Share your reports and pics from <a href="https://wiki.debian.org/ReleasePartyJessie">Debian jessie release parties</a> and we'll try to feature them in the next Debian Project News. #releasingjessie

