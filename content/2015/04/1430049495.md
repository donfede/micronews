Title: Releasing Debian jessie: color dmesg
Slug: 1430049495
Date: 2015-04-26 11:58:15
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/BScnASZ4TCyvNpwinaH4dA

Already upgraded to jessie? Then checkout the new colored dmesg output! #releasingjessie

