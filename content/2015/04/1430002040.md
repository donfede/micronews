Title: Releasing Debian jessie: Debian and nature
Slug: 1430002040
Date: 2015-04-25 22:47:20
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/A2D2yX63RVSDX89tNRFz9A

Debian is not a force of nature, <a href="https://lists.debian.org/debian-curiosa/2014/10/msg00009.html">despite its logo</a>! #releasingjessie

