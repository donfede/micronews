Title: Releasing Debian jessie: thanks for sharing
Slug: 1430052470
Date: 2015-04-26 12:47:50
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/Qs1NdaOZSuWDH5dU85FNpg

Thanks for spreading the word during the jessie release; repeats, replies, forwards, likes all appreciated. #releasingjessie

