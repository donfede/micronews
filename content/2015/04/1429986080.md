Title: Releasing Debian jessie: Beware of upgrade conflicts
Slug: 1429986080
Date: 2015-04-25 18:21:20
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/NR8H3VYBRrqul1-oP-VocQ

 Be aware that any non-Debian packages on your system may be removed during the upgrade because of conflicting dependencies.

