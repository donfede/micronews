Title: Releasing Debian jessie: systemd
Slug: 1430043662
Date: 2015-04-26 10:21:02
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/ZOn34HSJQ3isbFkDG-W8aw

The installation system now installs <a href="https://wiki.debian.org/systemd">systemd</a> as the default init system but <a href="https://wiki.debian.org/systemd#Installing_without_systemd">you can use sysvinit</a>. #releasingjessie

