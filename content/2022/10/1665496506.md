Title: Registration started for #MiniDebConf Palakkad 2022. Deadline for registration is November 1st. https://in2022.mini.debconf.org/register/ #Debian #DebianIndia #MDCPalakkad2022
Slug: 1665496506
Date: 2022-10-11 13:55
Author: Anupa Ann Joseph
Status: published

Registration started for #MiniDebConf Palakkad 2022. Deadline for registration is November 1st. [https://in2022.mini.debconf.org/register/](https://in2022.mini.debconf.org/register/) #Debian #DebianIndia #MDCPalakkad2022
