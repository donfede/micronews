Title: Bits from the Debian Account Managers (DAMs) https://lists.debian.org/debian-devel-announce/2022/10/msg00002.html
Slug: 1665426806
Date: 2022-10-10 18:33
Author: Laura Arjona Reina
Status: published

Bits from the Debian Account Managers (DAMs) [https://lists.debian.org/debian-devel-announce/2022/10/msg00002.html](https://lists.debian.org/debian-devel-announce/2022/10/msg00002.html)
