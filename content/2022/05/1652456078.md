Title: New Debian Developers and Maintainers (March and April 2022) https://bits.debian.org/2022/05/new-developers-2022-04.html
Slug: 1652456078
Date: 2022-05-13 15:34
Author: Jean-Pierre Giraud
Status: published

New Debian Developers and Maintainers (March and April 2022) [https://bits.debian.org/2022/05/new-developers-2022-04.html](https://bits.debian.org/2022/05/new-developers-2022-04.html)
