Title: Talks for Debian Reunion Hamburg will start now! See https://hamburg-2022.mini.debconf.org/ & enjoy!
Slug: 1653732952
Date: 2022-05-28 10:15
Author: Lee Garrett
Status: published

Talks for Debian Reunion Hamburg will start now! See [https://hamburg-2022.mini.debconf.org/](https://hamburg-2022.mini.debconf.org/) & enjoy!
