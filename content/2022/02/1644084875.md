Title: Perl 5.34 transition underway in unstable! https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html
Slug: 1644084875
Date: 2022-02-05 18:14
Author: Laura Arjona Reina
Status: published

Perl 5.34 transition underway in unstable! [https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html](https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html)
