Title: Have you missed any of your favourite talks in #DebConf22? You can still watch them: https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/ video team has you covered\!
Slug: 1658328234
Date: 2022-07-20 14:43
Author: Anupa Ann Joseph
Status: published

Have you missed any of your favourite talks in #DebConf22? You can still watch them: [https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/](https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/) video team has you covered\!
