Title: "What's new in APT this decade?" is happening at 9:00 UTC in Drini room and a BoF "GNOME metapackage reorganization" in Ereniku. A BoF by DebConf bursary team will happen at the same time in Mirusha (no streaming) https://debconf22.debconf.org/schedule/?block=2
Slug: 1658139993
Date: 2022-07-18 10:26
Author: Anupa Ann Joseph
Status: published

"What's new in APT this decade?" is happening at 9:00 UTC in Drini room and a BoF "GNOME metapackage reorganization" in Ereniku. A BoF by DebConf bursary team will happen at the same time in Mirusha (no streaming) [https://debconf22.debconf.org/schedule/?block=2](https://debconf22.debconf.org/schedule/?block=2)
