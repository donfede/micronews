Title: "Reproducible Builds for Bullseye, Bookworm and beyond - where we come from and where we are going" happens live in Lumbardhi and Debian installer and images team BoF at Ereniku from 13:00 UTC https://debconf22.debconf.org/schedule/?block=3
Slug: 1658242572
Date: 2022-07-19 14:56
Author: Anupa Ann Joseph
Status: published

"Reproducible Builds for Bullseye, Bookworm and beyond - where we come from and where we are going" happens live in Lumbardhi and Debian installer and images team BoF at Ereniku from 13:00 UTC [https://debconf22.debconf.org/schedule/?block=3](https://debconf22.debconf.org/schedule/?block=3)
