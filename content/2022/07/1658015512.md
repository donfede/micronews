Title: Debian Project News - July 16th, 2022: DebConf22 underway, bookworm freeze dates, DSAs, News on Debian releases, Elections and votes, Outreach, Events, Reports, Contributors and more! https://www.debian.org/News/weekly/2022/01/
Slug: 1658015512
Date: 2022-07-16 23:51
Author: Laura Arjona Reina
Status: published

Debian Project News - July 16th, 2022: DebConf22 underway, bookworm freeze dates, DSAs, News on Debian releases, Elections and votes, Outreach, Events, Reports, Contributors and more! [https://www.debian.org/News/weekly/2022/01/](https://www.debian.org/News/weekly/2022/01/)
