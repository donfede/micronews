Title: Talks for Day 2 of #DebConf22 start at 8:00 UTC with "Hello from keyring-maint" in Lumbardhi hall and "How Collabora's sysadmins use Debian" in Ereniku hall https://debconf22.debconf.org/schedule/?block=2
Slug: 1658137993
Date: 2022-07-18 09:53
Author: Anupa Ann Joseph
Status: published

Talks for Day 2 of #DebConf22 start at 8:00 UTC with "Hello from keyring-maint" in Lumbardhi hall and "How Collabora's sysadmins use Debian" in Ereniku hall [https://debconf22.debconf.org/schedule/?block=2](https://debconf22.debconf.org/schedule/?block=2)
