Title: Today is the #DebConf22 Day Trip. Conference attendees are spending some leisure time together, which helps to strengthen the community.
Slug: 1658395614
Date: 2022-07-21 09:26
Author: Laura Arjona Reina
Status: published

Today is the #DebConf22 Day Trip. Conference attendees are spending some leisure time together, which helps to strengthen the community.
