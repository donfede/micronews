Title:  #DebConf22 day 1 has ended, stay tuned tomorrow Monday 18, July 08:00 UTC. The schedule for the day is https://debconf22.debconf.org/schedule/?block=2 . Thank you to all of our attendants, contributors, viewers, and to our Debconf Organization and Video teams! See you tomorrow!
Slug: 1658082079
Date: 2022-07-17 18:21
Author: Laura Arjona Reina
Status: published

 #DebConf22 day 1 has ended, stay tuned tomorrow Monday 18, July 08:00 UTC. The schedule for the day is [https://debconf22.debconf.org/schedule/?block=2](https://debconf22.debconf.org/schedule/?block=2) . Thank you to all of our attendants, contributors, viewers, and to our Debconf Organization and Video teams! See you tomorrow!
