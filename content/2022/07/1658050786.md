Title: First day of DebConf! Have a look at the schedule for today https://debconf22.debconf.org/schedule/?block=1 and follow the live streaming from #DebConf22 website
Slug: 1658050786
Date: 2022-07-17 09:39
Author: Laura Arjona Reina
Status: published

First day of DebConf! Have a look at the schedule for today [https://debconf22.debconf.org/schedule/?block=1](https://debconf22.debconf.org/schedule/?block=1) and follow the live streaming from #DebConf22 website
