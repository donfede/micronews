Title: Daytrip at #DebConf22 is on July 21st Thursday, and those who are attending the conference in person can sign up for any of them at https://wiki.debian.org/DebConf/22/DayTrip If you need help with that, go to Front desk
Slug: 1658143924
Date: 2022-07-18 11:32
Author: Anupa Ann Joseph
Status: published

Daytrip at #DebConf22 is on July 21st Thursday, and those who are attending the conference in person can sign up for any of them at [https://wiki.debian.org/DebConf/22/DayTrip](https://wiki.debian.org/DebConf/22/DayTrip) If you need help with that, go to Front desk
