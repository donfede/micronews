Title: Salsa (salsa.debian.org, the Debian Gitlab instance) is back online https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html - thanks DSA and Salsa admins!
Slug: 1646175793
Date: 2022-03-01 23:03
Author: Laura Arjona Reina
Status: published

Salsa (salsa.debian.org, the Debian Gitlab instance) is back online [https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html](https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html) - thanks DSA and Salsa admins!
