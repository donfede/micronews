Title: Debian buster users: The 10.12 release includes an updated OpenSSL with signature algorithm check tightening - see the release point announcement for details https://www.debian.org/News/2022/2022032602
Slug: 1648320213
Date: 2022-03-26 18:43
Author: Laura Arjona Reina
Status: published

Debian buster users: The 10.12 release includes an updated OpenSSL with signature algorithm check tightening - see the release point announcement for details [https://www.debian.org/News/2022/2022032602](https://www.debian.org/News/2022/2022032602)
