Title: mentors.debian.net internationalization (and call for translators) https://lists.debian.org/debian-i18n/2022/11/msg00007.html
Slug: 1669233050
Date: 2022-11-23 19:50
Author: Laura Arjona Reina
Status: published

mentors.debian.net internationalization (and call for translators) [https://lists.debian.org/debian-i18n/2022/11/msg00007.html](https://lists.debian.org/debian-i18n/2022/11/msg00007.html)
