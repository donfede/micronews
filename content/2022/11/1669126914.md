Title: Misc Developer News (#58): anacron might be disabled if 2.3-33 was ever installed; riscv64 porterbox; porterbox DNS alias maintainers needed; debtags.d.o maintainers needed; lintian contributors needed; Experimental manual migration pseudo-excuses; CPU instruction selection documentation https://lists.debian.org/debian-devel-announce/2022/11/msg00001.html
Slug: 1669126914
Date: 2022-11-22 14:21
Author: Laura Arjona Reina
Status: published

Misc Developer News (#58): anacron might be disabled if 2.3-33 was ever installed; riscv64 porterbox; porterbox DNS alias maintainers needed; debtags.d.o maintainers needed; lintian contributors needed; Experimental manual migration pseudo-excuses; CPU instruction selection documentation [https://lists.debian.org/debian-devel-announce/2022/11/msg00001.html](https://lists.debian.org/debian-devel-announce/2022/11/msg00001.html)
