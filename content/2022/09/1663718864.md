Title: General Resolution: Non-free firmware: First call for votes https://lists.debian.org/debian-devel-announce/2022/09/msg00002.html
Slug: 1663718864
Date: 2022-09-21 00:07
Author: Laura Arjona Reina
Status: published

General Resolution: Non-free firmware: First call for votes [https://lists.debian.org/debian-devel-announce/2022/09/msg00002.html](https://lists.debian.org/debian-devel-announce/2022/09/msg00002.html)
