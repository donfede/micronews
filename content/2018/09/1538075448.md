Title: Call for mentors and project ideas for next Outreachy round https://lists.debian.org/debian-outreach/2018/09/msg00030.html
Slug: 1538075448
Date: 2018-09-27 19:10
Author: Laura Arjona Reina
Status: published

Call for mentors and project ideas for next Outreachy round [https://lists.debian.org/debian-outreach/2018/09/msg00030.html](https://lists.debian.org/debian-outreach/2018/09/msg00030.html)
