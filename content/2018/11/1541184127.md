Title: Rust now available on 14 architectures -- https://lists.debian.org/debian-devel-announce/2018/11/msg00000.html
Slug: 1541184127
Date: 2018-11-02 18:42
Author: Chris Lamb
Status: published

Rust now available on 14 architectures -- [https://lists.debian.org/debian-devel-announce/2018/11/msg00000.html](https://lists.debian.org/debian-devel-announce/2018/11/msg00000.html)
