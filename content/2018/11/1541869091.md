Title: Updated Debian 9: 9.6 released https://www.debian.org/News/2018/20181110
Slug: 1541869091
Date: 2018-11-10 16:58
Author: Laura Arjona Reina
Status: published

Updated Debian 9: 9.6 released [https://www.debian.org/News/2018/20181110](https://www.debian.org/News/2018/20181110)
