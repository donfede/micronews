Title: Debian is applying as organization for Google Summer of Code 2018! Join the Outreach Team to help making it a success https://wiki.debian.org/SummerOfCode2018
Slug: 1516646810
Date: 2018-01-22 18:46
Author: Laura Arjona Reina
Status: published

Debian is applying as organization for Google Summer of Code 2018! Join the Outreach Team to help making it a success [https://wiki.debian.org/SummerOfCode2018](https://wiki.debian.org/SummerOfCode2018)
