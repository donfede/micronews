Title: Debian Project News - December 2018: Package Salvaging, Reproducible Builds and the Software Conservancy, Rust on 14 Debian Archs, freeze timeline, and more! https://www.debian.org/News/weekly/2018/04/
Slug: 1544817777
Date: 2018-12-14 20:02
Author: Donald Norwood
Status: published

Debian Project News - December 2018: Package Salvaging, Reproducible Builds and the Software Conservancy, Rust on 14 Debian Archs, freeze timeline, and more! [https://www.debian.org/News/weekly/2018/04/](https://www.debian.org/News/weekly/2018/04/)
