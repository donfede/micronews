Title: More afternoon talks: Analysing Debian packages with Neo4j, Civilization runs on Debian and Debian Med BoF https://debconf18.debconf.org/schedule/?day=2018-07-29
Slug: 1532854165
Date: 2018-07-29 08:49
Author: Laura Arjona Reina
Status: published

More afternoon talks: Analysing Debian packages with Neo4j, Civilization runs on Debian and Debian Med BoF [https://debconf18.debconf.org/schedule/?day=2018-07-29](https://debconf18.debconf.org/schedule/?day=2018-07-29)
