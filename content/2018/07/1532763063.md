Title: More talks at #DebConf18 Open Day about Debian and other Free Software Projects, and a packaging workshop https://debconf18.debconf.org/schedule/open-day/
Slug: 1532763063
Date: 2018-07-28 07:31
Author: Laura Arjona Reina
Status: published

More talks at #DebConf18 Open Day about Debian and other Free Software projects as well as a packaging workshop -- [https://debconf18.debconf.org/schedule/open-day/](https://debconf18.debconf.org/schedule/open-day/)
