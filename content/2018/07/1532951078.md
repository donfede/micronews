Title: We have had the first session of Live Demos at #DebConf18 and "Building Debian-based system images" and "Autodeploy from salsa" https://debconf18.debconf.org/schedule/?day=2018-07-30
Slug: 1532951078
Date: 2018-07-30 11:44
Author: Laura Arjona Reina
Status: published

We have had the first session of Live Demos at #DebConf18 and "Building Debian-based system images" and "Autodeploy from salsa" [https://debconf18.debconf.org/schedule/?day=2018-07-30](https://debconf18.debconf.org/schedule/?day=2018-07-30)
