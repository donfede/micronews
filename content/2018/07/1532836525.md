Title: This morning at #DebConf18: Opening Ceremony, Software transparency (package security) and Debian Games -- https://debconf18.debconf.org/schedule/?day=2018-07-29
Slug: 1532836525
Date: 2018-07-29 03:55
Author: Laura Arjona Reina
Status: published

This morning at #DebConf18: Opening Ceremony, Software transparency (package security) and Debian Games -- [https://debconf18.debconf.org/schedule/?day=2018-07-29](https://debconf18.debconf.org/schedule/?day=2018-07-29)
