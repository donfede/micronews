Title: Videos from the recent MiniDebconf in Hamburg are now available -- https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg#Schedule
Slug: 1527530056
Date: 2018-05-28 17:54
Author: Chris Lamb
Status: published

Videos from the recent MiniDebconf in Hamburg are now available -- [https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg#Schedule](https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg#Schedule)
