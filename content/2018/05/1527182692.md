Title: Debian bug #900000 was just filed by Salvatore Bonaccorso -- https://bugs.debian.org/900000
Slug: 1527182692
Date: 2018-05-24 17:24
Author: Chris Lamb
Status: published

Debian bug #900000 was just filed by Salvatore Bonaccorso -- [https://bugs.debian.org/900000](https://bugs.debian.org/900000)
