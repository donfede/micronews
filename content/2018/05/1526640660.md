Title: The Hamburg MiniDebconf starts tomorrow (Sat 19th) -- see the schedule at https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg#Schedule and watch live via https://video.debconf.org #minidebconfhamburg
Slug: 1526640660
Date: 2018-05-18 10:51
Author: Chris Lamb
Status: published

The Hamburg MiniDebconf starts tomorrow (Sat 19th) -- see the schedule at [https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg#Schedule](https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg#Schedule) and watch live via [https://video.debconf.org](https://video.debconf.org) #minidebconfhamburg
