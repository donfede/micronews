Title: The 2nd day of the Hamburg MiniDebConf restarts at 10h00 UTC+0200 -- watch live at https://video.debconf.org
Slug: 1526805548
Date: 2018-05-20 08:39
Author: Chris Lamb
Status: published

The 2nd day of the Hamburg MiniDebConf restarts at 10h00 UTC+0200 -- watch live at [https://video.debconf.org](https://video.debconf.org)
