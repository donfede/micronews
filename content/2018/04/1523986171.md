Title: Debian Release team published a detailed roadmap for Debian Buster release -- https://lists.debian.org/debian-devel-announce/2018/04/msg00006.html
Slug: 1523986171
Date: 2018-04-17 17:29
Author: Martin Zobel-Helas
Status: published

Debian Release team published a detailed roadmap for Debian Buster release -- [https://lists.debian.org/debian-devel-announce/2018/04/msg00006.html](https://lists.debian.org/debian-devel-announce/2018/04/msg00006.html)
