Title: Debian Developer Jonathan Carter is publishing a "Package of the Day" video series -- https://www.youtube.com/playlist?list=PLIbuTLE8Wih4pBd8S9UjGDGQQXG4-a6Yw
Slug: 1524849006
Date: 2018-04-27 17:10
Author: Chris Lamb
Status: published

Debian Developer Jonathan Carter is publishing a "Package of the Day" video series -- [https://www.youtube.com/playlist?list=PLIbuTLE8Wih4pBd8S9UjGDGQQXG4-a6Yw](https://www.youtube.com/playlist?list=PLIbuTLE8Wih4pBd8S9UjGDGQQXG4-a6Yw)
