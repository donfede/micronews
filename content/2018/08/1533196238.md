Title: Afternoon plenary at #DebConf18: "That's a free software issue!" featuring Molly de Blanc & Karen Sandler https://debconf18.debconf.org/talks/38-thats-a-free-software-issue/
Slug: 1533196238
Date: 2018-08-02 07:50
Author: Laura Arjona Reina
Status: published

Afternoon plenary at #DebConf18: "That's a free software issue!" featuring Molly de Blanc & Karen Sandler [https://debconf18.debconf.org/talks/38-thats-a-free-software-issue/](https://debconf18.debconf.org/talks/38-thats-a-free-software-issue/)
