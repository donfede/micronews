Title: Several Debian designers created artwork to celebrate #DebianDay #Debian25years - These are by Valessio Brito https://salsa.debian.org/valessio-guest/DebianArt/tree/master/posters/25th
Slug: 1534460375
Date: 2018-08-16 22:59
Author: Laura Arjona Reina
Status: published

Several Debian designers created artwork to celebrate #DebianDay #Debian25years - These are by Valessio Brito [https://salsa.debian.org/valessio-guest/DebianArt/tree/master/posters/25th](https://salsa.debian.org/valessio-guest/DebianArt/tree/master/posters/25th)

![Debian is 25 years old by Valessio Brito](|static|/images/Debian25years-ValessioBrito.png)

