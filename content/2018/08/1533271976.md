Title: More morning talks: My crush on GNU Guix, Delta upgrades revisited, DSA (Debian System Administrators) BoF https://debconf18.debconf.org/schedule/?day=2018-08-03
Slug: 1533271976
Date: 2018-08-03 04:52
Author: Laura Arjona Reina
Status: published

More morning talks: My crush on GNU Guix, Delta upgrades revisited, DSA (Debian System Administrators) BoF [https://debconf18.debconf.org/schedule/?day=2018-08-03](https://debconf18.debconf.org/schedule/?day=2018-08-03)
