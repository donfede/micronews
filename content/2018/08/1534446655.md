Title: Several Debian designers created artwork to celebrate #DebianDay #Debian25years - see this one by Petrusko
Slug: 1534446655
Date: 2018-08-16 19:10
Author: Donald Norwood
Status: published

Several Debian designers created artwork to celebrate #DebianDay #Debian25years - see this one by Petrusko [https://lists.debian.org/debian-publicity/2018/08/msg00014.html](https://lists.debian.org/debian-publicity/2018/08/msg00014.html) 

![Debian is 25 years old by Petrusko](|static|/images/Debian25years-Petrusko.png)
