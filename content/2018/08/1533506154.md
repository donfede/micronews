Title:  #DebConf18 closes in Hsinchu and #DebConf19 dates announced https://bits.debian.org/2018/08/debconf18-closes.html
Slug: 1533506154
Date: 2018-08-05 21:55
Author: Laura Arjona Reina
Status: published

 #DebConf18 closes in Hsinchu and #DebConf19 dates announced [https://bits.debian.org/2018/08/debconf18-closes.html](https://bits.debian.org/2018/08/debconf18-closes.html)
